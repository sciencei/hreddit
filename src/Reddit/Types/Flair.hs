{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}

module Reddit.Types.Flair where

import GHC.Generics (Generic)
import Data.Aeson.Types (FromJSON(..), Value(Object), ToJSON)
import Data.HashMap.Strict (HashMap)
import Web.HttpApiData (ToHttpApiData(..))
import Data.Aeson ((.:), (.:?), withObject, withText)
import Data.Text (Text)
import qualified Data.Text as T
import Control.Applicative ((<|>))
import Data.Coerce (coerce)

import Reddit.Types.Misc (emptyTextAsNothing, Color, CssClass(..), ThingType(User'), Listing(..), IsThingType(..))
import Reddit.Types.Emoji
import Reddit.Types.User (User(..))

-- | Whether the flair is for users or for links (posts)
data FlairType = UserFlair
               | LinkFlair
               deriving (Show, Read, Eq, Generic)

instance ToHttpApiData FlairType where
    toQueryParam UserFlair = "USER_FLAIR"
    toQueryParam LinkFlair = "LINK_FLAIR"

-- | Where the flair appears relative to the link/username
data FlairPosition = PosLeft
                   | PosRight
                   deriving (Show, Read, Eq, Generic)

instance ToHttpApiData FlairPosition where
  toQueryParam = T.toLower . T.pack . drop 3 . show

instance FromJSON FlairPosition where
    parseJSON = withText "FlairPosition"
                  $ \s -> case s of
                           "left" -> pure PosLeft
                           "right" -> pure PosRight
                           otherwise -> fail $ T.unpack s <> " is not a valid flair position"

-- | Text color for a v2 flair
data FlairTextColor = Dark
                    | Light
                    deriving (Show, Read, Eq, Generic)
                
instance ToHttpApiData FlairTextColor where
    toQueryParam = T.toLower . T.pack . show

instance FromJSON FlairTextColor where
    parseJSON = withText "FlairTextColor"
                  $ \s -> case s of
                           "dark" -> pure Dark
                           "light" -> pure Light
                           otherwise -> fail $ T.unpack s <> " is not a valid flair text color"

newtype FlairTemplateId (ft :: FlairType) = FlairTemplateId Text
                        deriving (Show, Read, Eq, Generic, FromJSON, ToHttpApiData, ToJSON)

-- | Generic representation of a v1 or v2 flair
data Flair (ft :: FlairType)
    = Flair { text :: Maybe Text
            , id :: (FlairTemplateId ft)
            , position :: Maybe FlairPosition
            , textEditable :: Maybe Bool
            , cssClass :: Maybe CssClass
            } deriving (Show, Read, Eq, Generic)

instance FromJSON (Flair ft) where
    parseJSON = withObject "Flair" $ \r -> do
      text         <- r .:  "flair_text" <|> r .:? "text" >>= emptyTextAsNothing
      position     <- r .:? "flair_position" 
      textEditable <- r .:  "flair_text_editable" <|> r .:? "text_editable"
      cssClass     <- (fmap Css) <$> (r .:  "flair_css_class" <|> r .:? "css_class" >>= emptyTextAsNothing)
      id   <- r .:  "flair_template_id" <|> r .: "id"
      pure $ Flair{..}

-- | Flair returned for a user's own flair, which may not conform to a template
-- and so may have a null id, unlike other flair representations
data AdHocFlair (ft :: FlairType)
    = AdHocFlair { text :: Maybe Text
                 , id :: Maybe (FlairTemplateId ft)
                 , position :: Maybe FlairPosition
                 , textEditable :: Maybe Bool
                 , cssClass :: Maybe CssClass
                 } deriving (Show, Read, Eq, Generic)

instance FromJSON (AdHocFlair ft) where
    parseJSON = withObject "AdHocFlair" $ \r -> do
      text         <- r .:  "flair_text" <|> r .:? "text"
      position     <- r .:? "flair_position"
      textEditable <- r .:  "flair_text_editable" <|> r .:? "text_editable"
      cssClass     <- r .:  "flair_css_class" <|> r .:? "css_class"
      id   <- r .:  "flair_template_id" <|> r .:? "id"
      pure $ AdHocFlair{..}

-- | Whether a flair is a simple text flair or contains 'RichText'
data FlairV2Type = Text
                 | RichText
                 deriving (Show, Read, Eq, Generic)

instance FromJSON FlairV2Type where
    parseJSON = withText "FlairV2Type"
        $ \s -> case s of
                  "text"     -> pure Text
                  "richtext" -> pure RichText
                  otherwise  -> fail $ T.unpack s <> " is not a valid flairv2 type"

-- | A v2 flair
data FlairV2 (ft :: FlairType)
    = FlairV2 { backgroundColor :: Maybe Color
              , textColor      :: FlairTextColor
              , text :: Maybe Text
              , modOnly :: Bool
              , textEditable :: Bool -- ^ Whether (normal users) can edit the flairtext
              , id :: (FlairTemplateId ft)
              , richtext :: [RichText] -- ^ Breakdown of the flair into text and emoji pieces
              , typ :: FlairV2Type
              } deriving (Show, Read, Eq, Generic)

instance FromJSON (FlairV2 ft) where
    parseJSON = withObject "FlairV2" $ \r -> do
      text         <- r .:? "text" >>= emptyTextAsNothing
      textColor    <- r .:  "text_color" <|> r .: "textColor"
      backgroundColor  <- r .: "background_color" <|> r .:? "backgroundColor"
      textEditable <- r .: "text_editable" <|> r .: "textEditable"
      modOnly <- r .: "mod_only" <|> r .: "modOnly"
      id   <- r .: "id"
      typ <- r .: "type"
      richtext <- r .: "richtext"
      pure $ FlairV2{..}

-- | Available flairing options
data FlairSelector (ft :: FlairType)
    = FlairSelector { current :: (AdHocFlair ft) -- ^ Current flair, if any
                    , choices :: [Flair ft] -- ^ Available options
                    } deriving (Show, Read, Eq, Generic)

instance FromJSON (FlairSelector ft)

-- | A portion of a v2 flair representing either text or an emoji
data RichText = REmoji (EmbeddedEmoji)
              | RText Text
              deriving (Show, Read, Eq, Generic)

instance FromJSON RichText where
    parseJSON = withObject "RichText" $ \o -> do
      typ <- o .: "e"
      case typ of
        "emoji" -> REmoji <$> parseJSON (Object o)
        "text" -> RText <$> o .: "t"
        t      -> fail $ "Unrecognized rich object type " <> T.unpack t

-- | A flair entry in the old style
data OldFlair = OldFlair
              { name :: User
              , text :: Maybe Text
              , cssClass :: Maybe CssClass
              } deriving (Show, Read, Eq, Generic)

instance FromJSON OldFlair where
  parseJSON = withObject "OldFlair" $ \o -> do
    name <- o .: "user"
    text <- o .:? "flair_text" >>= emptyTextAsNothing
    cssClass <- coerce <$> (o .:? "flair_css_class" >>= emptyTextAsNothing)
    pure $ OldFlair{..}

instance ToHttpApiData [OldFlair] where
  toQueryParam = T.intercalate "\r\n" . map indiv
    where
      indiv (OldFlair n t c) =
        T.intercalate "," $ map (maybe "" (\i -> T.concat ["\"", i, "\""]))
                            $ [Just $ unUser n, t, coerce c]

-- | Result of setting a flair via massFlairUser
data FlairUpdateResult = FlairUpdateResult
  { status :: Text -- ^ status message like "removed flair for <user>"
  , errors :: HashMap Text Value 
  -- ^ errors; left as an 'Object' because it only returns 
  --   fields for errors that happen, making creating an
  --   exhaustive model infeasible
  , warnings :: HashMap Text Value
  -- ^ warnings; left as an 'Object' because it only returns 
  --   fields for errors that happen, making creating an
  --   exhaustive model infeasible
  , ok :: Bool -- ^ whether an update completed successfully
  } deriving (Show, Read, Eq, Generic)

instance FromJSON FlairUpdateResult

instance {-# OVERLAPS #-} FromJSON (Listing OldFlair) where
    parseJSON = withObject "Flair Listing" $ \o -> do
      Listing
        <$> o .:? "next"
        <*> o .:? "prev"
        <*> o .: "users"

instance IsThingType OldFlair where
  type AsThingType OldFlair = User'
  validateThing = validateThing @User
