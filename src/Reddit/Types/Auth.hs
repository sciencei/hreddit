{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Reddit.Types.Auth where

import GHC.Generics (Generic)
import Data.Aeson
import Data.Text (Text)
import Data.Char (isUpper, toLower)
import Web.HttpApiData (ToHttpApiData(..))

data AuthToken = AuthToken
               { accessToken :: Text
               , expiresIn   :: Int
               , scope       :: Text --TODO: Proper scope type
               , tokenType   :: Text --TODO: Proper tokenType type?
               } deriving (Show, Read, Eq, Generic)

instance FromJSON AuthToken where
    parseJSON = genericParseJSON (defaultOptions {fieldLabelModifier = camelToSnake})

data AuthType = ClientCredentials
              | Password
              deriving (Show, Read, Eq, Generic)

instance ToHttpApiData AuthType where
    toUrlPiece ClientCredentials = "client_credentials"
    toUrlPiece Password          = "password"

camelToSnake :: String -> String
camelToSnake = foldr (\x acc -> if isUpper x then '_' : toLower x : acc else x : acc) []
