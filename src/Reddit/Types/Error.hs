{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Reddit.Types.Error where

import Servant.Client.Core (Response, responseBody)
import qualified Servant.Client.Core as S
import GHC.Generics (Generic(..))
import GHC.Exception.Type (Exception(..), SomeException)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Data.ByteString.Lazy (toStrict)
import Network.HTTP.Client (Request)

data RedditError
    = Other Response -- ^ catch-all
    | Unauthorized Text -- ^ not logged in
    | Forbidden Text -- ^ insufficient permissions
    | NotFound Request -- ^ link not found
    | ParseError Text Response -- ^ failed to parse the response body correctly
    | ConnectionError SomeException -- ^ connection error; no response
    | BadRequest Text Request -- ^ fairly generic error message thrown sometimes
    deriving (Show, Generic)

instance Exception RedditError

fromServantError :: S.ClientError -> RedditError
fromServantError (S.FailureResponse _ r) = Other r --TODO: add request to Other
fromServantError (S.DecodeFailure t r) = ParseError t r
fromServantError (S.UnsupportedContentType _ r) = Other r
fromServantError (S.InvalidContentTypeHeader r) = Other r
fromServantError (S.ConnectionError t) = ConnectionError t

fromRequestStatusResponse :: Request -> Int -> Response -> RedditError
fromRequestStatusResponse request status response
  = case status of
      400 -> BadRequest body request
      401 -> Unauthorized body
      403 -> if "Request forbidden by administrative rules." `T.isInfixOf` body
               then Unauthorized body
               else Forbidden body
      404 -> NotFound request
      _   -> Other response

  where body = decodeUtf8 . toStrict . responseBody $ response
