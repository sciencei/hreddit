{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE LambdaCase #-}

module Reddit.Servant.Emoji
-- ( EmojiApi(..)
-- ) where
 where

import GHC.Generics (Generic)
import Servant.API ((:>), Delete, Post, Get, JSON, mimeUnrender)
import Servant.API.Generic ((:-), ToServant, AsApi, fromServant, toServant)
import Servant.API.NamedArgs (RequiredNamedParam, NamedCapture, OptionalNamedParam)
import Servant.Client.Core ( HasClient(..), RunClient(..)
                           , defaultRequest, RequestF(..)
                           , ResponseF(responseBody)
                           , ClientError(DecodeFailure)
                           )
import Servant.Multipart
import Servant.Client.Generic (AsClientT)
import Data.Proxy (Proxy(..))
import qualified Data.Text.Encoding as TE
import Named ((:!), pattern Arg, (!), (:?))
import Network.HTTP.Types.Method (methodPost)
import Network.HTTP.Media.MediaType ((//), (/:))
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as BL

import Reddit.Types.Misc
import Reddit.Types.Emoji
import Reddit.Servant.Internal.Misc

-- | Routes dealing with emojis. Called through
-- 'Reddit.Servant.Reddit.RedditApi', all these endpoints have
-- "\/api\/v1\/{subreddit}" appended to the beginning
data EmojiApi route = EmojiApi
  { 
    -- | #deleteEmoji#
    --
    --   [@Endpoint@]
    --       <https://www.reddit.com/dev/api/#DELETE_api_v1_{subreddit}_emoji_{emoji_name} DELETE /emoji/{emoji_name}>
    --   [@Description@]
    --       Deletes an emoji from the subreddit.
    --   [@Required Parameters@]
    --
    --       [@emoji_name :: 'EmojiName'@] The name of the emoji to delete
    --
    --   [@Optional Parameters@]
    --       None
    --   [@Returns@]
    --       'Value'
    --   [@Notes@]
    --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user does not have
    --       the right permissions. Throws a 'Reddit.Types.Error.BadRequest' if the emoji
    --       to be deleted does not exist (lower precedence than 'Reddit.Types.Error.Forbidden'.
    deleteEmoji :: route :- "emoji" :> NamedCapture "emoji_name" EmojiName :> Delete '[JSON] Errors
  -- | #getEmojis#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#GET_api_v1_{subreddit}_emojis_all GET /emojis/all>
  --   [@Description@]
  --       Gets all the emojis for the subreddit, including the reddit emojis if available.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'EmojiSet'
  --   [@Notes@]
  , getEmojis :: route :- "emojis" :> "all" :> Get '[JSON] EmojiSet
  -- | #uploadEmoji#
  --
  --   [@Endpoint@]
  --       No direct correspondence
  --   [@Description@]
  --       Uploads an emoji to the subreddit.
  --   [@Required Parameters@]
  --
  --       [@name :: EmojiName@] The name of the emoji
  --       [@image :: EncodedFile@] The image for the emoji
  --
  --   [@Optional Parameters@]
  --
  --       [@mod_flair_only :: Bool@] If the emoji can only be used in mod-controlled flairs (default: false)
  --       [@post_flair_allowed :: Bool@] If the emoji can be used in post (link) flairs (default: true)
  --       [@user_flair_allowed :: Bool@] If the emoji can be used in user flairs (default: true)
  --
  --   [@Returns@]
  --       'Errors'
  --   [@Notes@]
  --       This endpoint is a wrapper around 'getEmojiLease', 'uploadEmojiToS3Bucket', and 'setEmojiFromS3Bucket'.
  , uploadEmoji :: route :- EmojiUpload
  -- | #setEmojiSize#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#POST_api_v1_{subreddit}_emoji_custom_size POST /emoji_custom_size>
  --   [@Description@]
  --       Sets a custom emoji size on the subreddit.
  --   [@Required Parameters@]
  --
  --       [@width :: 'Int'@] The emoji width (between 1 and 40 px)
  --       [@height :: 'Int'@] The emoji height (between 1 and 40 px)
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'Errors'
  --   [@Notes@]
  --       See 'defaultEmojiSize' to reset the emoji size to the default.
  , setEmojiSize :: route :- "emoji_custom_size" :> RequiredNamedParam "height" Int
                                                 :> RequiredNamedParam "width" Int
                                                 :> Post '[JSON] Errors
  -- | #defaultEmojiSize#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#POST_api_v1_{subreddit}_emoji_custom_size POST /emoji_custom_size>
  --   [@Description@]
  --       Resets the emoji size on the subreddit to the default (currently 15x15px).
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'Errors'
  --   [@Notes@]
  --       See 'setEmojiSize' to set the emoji size to something else.
  , defaultEmojiSize :: route :- "emoji_custom_size" :> Post '[JSON] Errors
  -- | #getEmojiLease#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#POST_api_v1_{subreddit}_emoji_asset_upload_s3.json POST /emoji_asset_upload_s3.json?mimetype=image>
  --   [@Description@]
  --       Acquires an upload lease to an s3 temp bucket to upload an emoji image to. You probably want 'uploadEmoji' instead, which
  --       wraps this, 'uploadEmojiToS3Bucket', and 'setEmojiFromS3Bucket' into one nice convenient function.
  --   [@Required Parameters@]
  --
  --       [@filepath :: 'FilePath'@] The path to the file to upload
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'S3Lease'
  --   [@Notes@]
  --       
  , getEmojiLease :: EmojiLeaseEndpoint route
  -- | #uploadEmojiToS3Bucket#
  --
  --   [@Endpoint@]
  --       No direct correspondence
  --   [@Description@]
  --       Uploads an emoji to an S3 bucket with the given lease. You almost definitely want 'uploadEmoji' instead,
  --       which wraps 'getEmojiLease', this, and 'setEmojiFromS3Bucket' into one nice convenient function.
  --   [@Required Parameters@]
  --
  --       [@lease :: S3Lease@] Lease for the S3 bucket
  --       [@image :: EncodedFile@] The image for the emoji
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'S3Lease'
  --   [@Notes@]
  --       This endpoint actually doesn't call the reddit api, instead calling an endpoint specified by 'action'.
  , uploadEmojiToS3Bucket :: S3UploadEndpoint route
  -- | #setEmojiFromS3Bucket#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#POST_api_v1_{subreddit}_emoji.json POST /emoji.json>
  --   [@Description@]
  --       Adds an emoji to the subreddit using the image from the S3 key. You almost definitely want
  --       'uploadEmoji' instead, which wraps 'getEmojiLease', 'uploadEmojiToS3Bucket', and this into
  --       one nice convenient function.
  --   [@Required Parameters@]
  --
  --       [@s3_key :: 'S3Key'@]
  --       [@name :: 'EmojiName'@]
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'Errors'
  --   [@Notes@]
  , setEmojiFromS3Bucket :: RedditS3UploadEndpoint route
  } deriving (Generic)

instance (RunClient m, HasClient m (ToServant EmojiApi AsApi)) => HasClient m (EmojiApi (AsId as)) where
    type Client m (EmojiApi (AsId as)) = EmojiApi (AsClientT m)
    clientWithRoute m _ = fromServant . clientWithRoute m (Proxy @(ToServant EmojiApi AsApi))
    hoistClientMonad m _ nt = fromServant . hoistClientMonad m (Proxy @(ToServant EmojiApi AsApi)) nt . toServant

instance (RunClient m, HasClient m (MkRaw (ToServant EmojiApi AsApi)))
         => HasClient m (EmojiApi (AsRaw as)) where
    type Client m (EmojiApi (AsRaw as)) = EmojiApi (AsRaw (AsClientT m))
    clientWithRoute m _ = fromServant . clientWithRoute m (Proxy @(MkRaw (ToServant EmojiApi AsApi)))
    hoistClientMonad m _ nt = fromServant . hoistClientMonad m (Proxy @(MkRaw (ToServant EmojiApi AsApi))) nt . toServant

-- | Separated out to be easier to call in 'uploadEmoji'
type EmojiLeaseEndpoint route = route :- "emoji_asset_upload_s3.json" :> SuppliedParam "mimetype" "image"
                                                                 :> RequiredNamedParam "filepath" FilePath
                                                                 :> Post '[JSON] S3Lease

-- | Separated out to be easier to call in 'uploadEmoji'
type S3UploadEndpoint route = route :- S3Upload

-- | Separated out to be easier to call in 'uploadEmoji'
type RedditS3UploadEndpoint route 
  = route :- "emoji.json" :> RequiredNamedParam "name" EmojiName
                          :> RequiredNamedParam "s3_key" S3Key
                          :> OptionalNamedParam "mod_flair_only" Bool
                          :> OptionalNamedParam "post_flair_allowed" Bool
                          :> OptionalNamedParam "user_flair_allowed" Bool
                          :> Post '[JSON] Errors

-- | Bespoke combinator for the S3 upload
data S3Upload

instance (RunClient m) => HasClient m S3Upload where

  type Client m (S3Upload) = ("image" :! EncodedFile) -> ("lease" :! S3Lease) -> m S3Key

  clientWithRoute _ _ _ (Arg ef) (Arg lease) = do
    let 
      boundary' = boundary ef
      urlPath = TE.encodeUtf8 . action $ lease
      addFile (MultipartData is fs) = MultipartData is (encodedFileToFileData "file" ef : fs)
      reqBody = multipartToBody boundary' . addFile . toMultipart $ lease
      reqBodyType = ("multipart" // "form-data" /: ("boundary", BL.toStrict boundary'))
      req = defaultRequest { requestPath = ""
                           , requestHeaders = [ ("FAKE-HEADER-ACTUALLY-HOST", urlPath)
                                              ]
                           , requestMethod = methodPost
                           , requestBody = Just (reqBody, reqBodyType)
                           , requestAccept = ["application" // "xml"]
                           }
    response <- runRequest req
    case mimeUnrender (Proxy @XML) (responseBody response) of
      Left err -> throwClientError $ DecodeFailure (T.pack err) response
      Right val -> pure val

  hoistClientMonad _ _ f ma = \e l -> f (ma e l)

-- | Bespoke combinator for all the steps of uploading a new emoji
data EmojiUpload

instance (RunClient m
         , HasClient m (S3UploadEndpoint AsApi)
         , Client m (S3UploadEndpoint AsApi) ~ (("image" :! EncodedFile) -> ("lease" :! S3Lease) -> m S3Key)
         , HasClient m (EmojiLeaseEndpoint AsApi)
         , Client m (EmojiLeaseEndpoint AsApi) ~ (("filepath" :! FilePath) -> m S3Lease)
         , HasClient m (RedditS3UploadEndpoint AsApi)
         , Client m (RedditS3UploadEndpoint AsApi) ~ (("name" :! EmojiName) -> ("s3_key" :! S3Key) 
                                                                            -> ("mod_flair_only" :? Bool)
                                                                            -> ("post_flair_allowed" :? Bool)
                                                                            -> ("user_flair_allowed" :? Bool)
                                                                            -> m Errors)
         ) => HasClient m EmojiUpload where

  type Client m (EmojiUpload) =  ("image" :! EncodedFile) 
                              -> ("name" :! EmojiName) 
                              -> ("mod_flair_only" :? Bool)
                              -> ("post_flair_allowed" :? Bool)
                              -> ("user_flair_allowed" :? Bool)
                              -> m Errors

  clientWithRoute pm _ req (Arg image) (Arg name) mf pf uf = do
    let 
      r1 = clientWithRoute pm (Proxy @(EmojiLeaseEndpoint AsApi)) req
      r2 = clientWithRoute pm (Proxy @(S3UploadEndpoint AsApi)) req
      r3 = clientWithRoute pm (Proxy @(RedditS3UploadEndpoint AsApi)) req
      path' = path image
    el <- r1 ! #filepath path'
    s3 <- r2 ! #image image ! #lease el
    (r3 ! #name name ! #s3_key s3) mf pf uf

  hoistClientMonad _ _ f ma = \i n m p u  -> f (ma i n m p u)
