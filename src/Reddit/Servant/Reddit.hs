{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}

module Reddit.Servant.Reddit where

import GHC.Generics
import Servant.API ((:>), BasicAuth, BasicAuthData, Capture, Post, JSON)
import Servant.API.Generic ( (:-), GenericMode(..)
                           )
import Servant.API.NamedArgs
import Data.Text (Text)

import Reddit.Types.Subreddit
import Reddit.Types.Auth
import Reddit.Servant.Subreddit
import Reddit.Servant.Emoji
import Reddit.Servant.Flair

data RedditApi route = RedditApi
  { withSub :: route :- "r" :> Capture "subreddit" SubredditName :> SubSpecificApi route
  , auth    :: route :- "api" :> "v1" :> "access_token"
                     :> BasicAuth "" BasicAuthData
                     :> RequiredNamedParam "grant_type" AuthType
                     :> OptionalNamedParam "username" Text
                     :> OptionalNamedParam "password" Text
                     :> Post '[JSON] AuthToken
  , flair :: route :- FlairApi route
  , emoji :: route :- "api" :> "v1" :> Capture "Subreddit" SubredditName :> EmojiApi route
  } deriving (Generic)
