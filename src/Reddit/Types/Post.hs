{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}

module Reddit.Types.Post where

import Reddit.Types.Misc
import Data.Text (Text)
import GHC.Generics (Generic)
import Data.Aeson.Types (FromJSON(..), parseMaybe)
import Data.Aeson ((.:), (.:?), withObject, withText)
import qualified Data.Text as T

data Post = Post
  { title :: Text
  , isRedditMediaDomain :: Bool -- ^ whether any media is hosted on Reddit
  , richtextMode :: Maybe RichtextMode -- ^ whether the post was made using markdown or the richtext editor
  , mediaEmbed :: Maybe MediaEmbed
  , secureMediaEmbed :: Maybe SecureMediaEmbed
  , id :: Thing Post'
  , subreddit :: Text
  , created :: UTCPosix
  , linkFlairText :: Maybe Text
  , isSpoiler :: Bool
  , score :: Int
  , numComments :: Int
  , permalink :: Text
  } deriving (Show, Read, Eq, Generic)

data MediaEmbed = MediaEmbed
  { height :: Int
  , scrolling :: Bool
  , content :: Text
  , width :: Int
  } deriving (Show, Read, Eq, Generic, FromJSON)

data SecureMediaEmbed = SecureMediaEmbed
  { height :: Int
  , scrolling :: Bool
  , content :: Text
  , width :: Int
  , mediaDomainUrl :: Text
  } deriving (Show, Read, Eq, Generic, FromJSON)

data RichtextMode = Markdown
                  | Richtext
                  deriving (Show, Read, Eq, Generic)

instance FromJSON RichtextMode where
    parseJSON = withText "RichtextMode" $ \case
      "markdown" -> pure Markdown
      "richtext" -> pure Richtext

instance FromJSON Post where
    parseJSON = withObject "Post" $ \r -> do
      _raw <- r .: "data"
      isRedditMediaDomain <- _raw .: "is_reddit_media_domain"
      secureMediaEmbed <- parseMaybe parseJSON <$> _raw .: "secure_media_embed"
      mediaEmbed <- parseMaybe parseJSON <$> _raw .: "media_embed"
      richtextMode <- _raw .:? "rte_mode"
      title         <- _raw .: "title"
      id            <- _raw .: "name"
      created       <- _raw .: "created_utc"
      isSpoiler     <- _raw .: "spoiler"
      linkFlairText <- _raw .:? "link_flair_text"
      subreddit     <- _raw .: "subreddit"
      score         <- _raw .: "score"
      numComments   <- _raw .: "num_comments"
      permalink     <- T.append "https://reddit.com" <$> (_raw .: "permalink")
      pure $ Post{..}

instance IsThingType Post where
    type AsThingType Post = Post'
    validateThing = T.isPrefixOf "t3_" 
