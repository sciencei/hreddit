{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE UndecidableInstances       #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE MultiWayIf                 #-}
{-# LANGUAGE OverloadedLabels                 #-}
{-# LANGUAGE ViewPatterns                 #-}
{-# LANGUAGE TemplateHaskell                 #-}
{-# LANGUAGE KindSignatures                 #-}

module Reddit.Reddit where

import Prelude hiding (throw)

import Servant.Client.Core (addHeader, RunClient(..), Request, Response, ClientError(..)
                           , StreamingResponse, requestPath, requestHeaders
                           , clientIn, hoistClientMonad
                           )
import Polysemy (makeSem, Member, Members, Sem, Lift, runM, sendM, reinterpret)
import Polysemy.Error (Error, throw, runError)
import Control.Concurrent.STM.TVar (modifyTVar', TVar, newTVarIO, readTVar, writeTVar)
import Control.Monad.STM (STM, atomically)
import Control.Monad (when)
import GHC.Generics (Generic)
import Data.Text (Text)
import Servant.Client.Generic (AsClientT)
import Servant.API hiding (addHeader, Header)
import Servant.API.Generic (fromServant, ToServant, AsApi)
import Servant.Client (BaseUrl(..), Scheme(..))
import Servant.Client.Internal.HttpClient (requestToClientRequest, catchConnectionError, clientResponseToResponse)
import Servant.Client.NamedArgs
import qualified Network.HTTP.Client as Client
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Network.HTTP.Types (statusCode)
import Data.ByteString.Builder (toLazyByteString)
import qualified Data.ByteString as BS
import Control.Concurrent (threadDelay)
import Named
import Data.Maybe (fromJust)
import Debug.Trace (trace)
import Control.Exception (throwIO)
import qualified Data.Sequence as Seq
import Data.Foldable (toList)
import Data.ByteString.Char8 (unpack)
import Data.Time.Clock (getCurrentTime)
import Data.Proxy (Proxy(..))

import Reddit.Servant.Reddit
import Reddit.Types.Error
import Reddit.Types.Auth
import Reddit.Servant.Internal.Misc (requestToClientRequestUnchunked, AsId, AsRaw)

type ModelClient m = RedditApi (AsId (AsClientT m))
type RawClient m = RedditApi (AsRaw (AsClientT m))

data RedditClient (m :: * -> *) a where
    RunRequest :: Request -> RedditClient m Response
    StreamingRequest :: Request -> RedditClient m StreamingResponse
    ThrowClientError :: ClientError -> RedditClient m a
    RunAuth :: Request -> RedditClient m Response
    RunExt :: Request -> RedditClient m Response

makeSem ''RedditClient

instance Member RedditClient r => RunClient (Sem r) where
    runRequest = Reddit.Reddit.runRequest
    throwClientError = Reddit.Reddit.throwClientError

data RedditEnv = RedditEnv
               { authorization     :: TVar RedditAuth
               , userAgent         :: Text
               , requestsRemaining :: TVar Int
               , resetTime         :: TVar Int
               , manager           :: Client.Manager
               , cookieJar         :: (TVar Client.CookieJar)
               } deriving (Generic)

data RedditAuth = RedditAuth
                { token        :: Maybe ExpiringToken
                , clientId     :: Maybe BS.ByteString
                , clientSecret :: Maybe BS.ByteString
                , username     :: Maybe Text
                , password     :: Maybe Text
                } deriving (Show, Read, Eq, Generic)

tokenAuth :: Text -> RedditAuth
tokenAuth t = RedditAuth (Just $ ExpiringToken t) Nothing Nothing Nothing Nothing

applicationAuth :: BS.ByteString -> BS.ByteString -> RedditAuth
applicationAuth cid csecret = RedditAuth Nothing (Just cid) (Just csecret) Nothing Nothing

userAuth :: BS.ByteString -> BS.ByteString -> Text -> Text -> RedditAuth
userAuth cid csecret user pass = RedditAuth Nothing (Just cid) (Just csecret) (Just user) (Just pass)

data ExpiringToken = ExpiringToken { getToken :: Text }
                   deriving (Show, Read, Eq)

reddit :: Member RedditClient r
       => ModelClient (Sem r)
reddit = fromServant $ clientIn (Proxy :: Proxy (ToServant RedditApi (AsId AsApi))) (Proxy :: Proxy (Sem r))

rawReddit :: Member RedditClient r
          => RawClient (Sem r)
rawReddit = fromServant $ clientIn (Proxy :: Proxy (ToServant RedditApi (AsRaw AsApi))) (Proxy :: Proxy (Sem r))

redditIO :: Text -> RedditAuth -> RedditApi (AsId (AsClientT IO))
redditIO agent rauth = fromServant
                     $ hoistClientMonad m api (\a -> (runM . runError $ runRedditIO agent rauth a) >>= either throwIO pure)
                     $ clientIn api m
  where
    m = Proxy :: Proxy (Sem '[RedditClient, Lift IO])
    api = Proxy :: Proxy (ToServant RedditApi (AsId AsApi))

rawRedditIO :: Text -> RedditAuth -> RedditApi (AsRaw (AsClientT IO))
rawRedditIO agent rauth = fromServant
                        $ hoistClientMonad m api (\a -> (runM . runError $ runRedditIO agent rauth a) >>= either throwIO pure)
                        $ clientIn api m
  where
    m = Proxy :: Proxy (Sem '[RedditClient, Lift IO])
    api = Proxy :: Proxy (ToServant RedditApi (AsRaw AsApi))

runRedditIO :: Member (Lift IO) r 
            => Text 
            -> RedditAuth 
            -> Sem (RedditClient ': r) a
            -> Sem (Error RedditError ': r) a
runRedditIO agent rauth cli = do
    man   <- sendM $ Client.newManager tlsManagerSettings
    reqs  <- sendM $ newTVarIO 60
    rtime <- sendM $ newTVarIO 60
    authv <- sendM $ newTVarIO rauth
    cookies <- sendM $ newTVarIO (Client.createCookieJar [])
    let renv = RedditEnv authv agent reqs rtime man cookies
    runRedditIO' renv cli

runRedditIO' :: Member (Lift IO) r
             => RedditEnv
             -> Sem (RedditClient ': r) a
             -> Sem (Error RedditError ': r) a
runRedditIO' renv cli = reinterpret go cli
  where
    withRateLimiting :: Member (Lift IO) r
                     => Client.Manager 
                     -> Client.Request 
                     -> Sem (Error RedditError ': r) (Int, Response)
    withRateLimiting man req = do
      (reqs, rtime) <- sendM $ atomically $ do
        reqs  <- readTVar $ requestsRemaining renv
        rtime <- readTVar $ resetTime renv
        pure $ (reqs, rtime)
      when (reqs < 1) $ sendM $ threadDelay $ 1000 * 1000 * rtime
      response <- sendM $ catchConnectionError $ Client.httpLbs req man
      case response of
        Left err -> throw (fromServantError err)
        Right response -> do
          let headers = Client.responseHeaders response
              remaining = lookup "x-ratelimit-remaining" headers
              reset     = lookup "x-ratelimit-reset" headers
              fRound :: (Integral a) => Float -> a
              fRound    = round
              setVarFromHeader :: (Integral a) => TVar a -> Maybe BS.ByteString -> STM ()
              setVarFromHeader v = maybe (pure ()) 
                                         (either (pure . const ()) (writeTVar v) . (fRound <$>) . parseHeader)
          sendM $ do
            now <- getCurrentTime
            atomically $ modifyTVar' (cookieJar renv) (fst . Client.updateCookieJar response req now)
          sendM $ atomically $ do
            setVarFromHeader (requestsRemaining renv) remaining
            setVarFromHeader (resetTime         renv) reset
          pure $ (statusCode $ Client.responseStatus response, clientResponseToResponse id response)
    go :: Member (Lift IO) r
       => RedditClient m a 
       -> Sem (Error RedditError ': r) a
    go (RunRequest req)
      | (toLazyByteString (requestPath req) == "/api/v1/access_token") = go (RunAuth req)
      | any (\(h, _) -> h == "FAKE-HEADER-ACTUALLY-HOST") (requestHeaders req) = go (RunExt req)
      | otherwise = do
          let
            baseUrl = BaseUrl Https "oauth.reddit.com" 443 ""
            man     = manager renv
            agent   = userAgent renv
          authv <- sendM $ atomically $ readTVar (authorization renv)
          let
            t             = getToken <$> token authv
            headedRequest = addHeader "User-Agent" agent
                            $ maybe req (\t -> addHeader "Authorization" ("bearer " <> t) req) (t)
            creq           = requestToClientRequest baseUrl headedRequest
          clientRequest <- sendM $ do
                             now <- getCurrentTime
                             atomically $ do
                               oldCookies <- readTVar (cookieJar renv)
                               let (newReq, newCookies) = 
                                     Client.insertCookiesIntoRequest creq oldCookies now
                               writeTVar (cookieJar renv) newCookies
                               pure newReq
          (status, response) <- withRateLimiting man $ requestToClientRequest baseUrl headedRequest
          if | status >= 200 && status <= 300 -> do
                trace (show clientRequest) (pure ())
                pure response
             | otherwise ->
                case (fromRequestStatusResponse clientRequest status response) of
                  Unauthorized t ->
                    case (clientId authv) of
                      Nothing -> throw $ Unauthorized t
                      Just _ -> do
                        let
                          authBase = auth reddit (BasicAuthData (fromJust $ clientId authv) (fromJust $ clientSecret authv))
                          authReq  = case (password authv) of
                                       Nothing -> authBase ! #grant_type ClientCredentials
                                                           ! defaults
                                       Just p -> authBase ! #grant_type Password
                                                          ! #username (fromJust $ username authv)
                                                          ! #password p
                        etoken <- sendM . runM . runError $ runRedditIO' renv authReq
                        case etoken of
                          Left err -> throw err
                          Right tok -> do
                             sendM $ atomically 
                                       $ writeTVar (authorization renv)
                                                   authv { token = Just $ ExpiringToken (accessToken tok) }
                             go (RunRequest req)
                  e -> throw e
    go (RunAuth req) = do
      let baseUrl = BaseUrl Https "www.reddit.com" 443 ""
          req'    = requestToClientRequest baseUrl (addHeader "User-Agent" (userAgent renv) req)
      (status, response) <- withRateLimiting (manager renv) $ req'
      if (status < 200 || status > 300) then throw $ fromRequestStatusResponse req' status response
                                        else pure response
    go (RunExt req) = do
      let
        (unpack . snd . last . toList -> burl, headers) = Seq.partition
                                             (\(h, _) -> h == "FAKE-HEADER-ACTUALLY-HOST")
                                             (requestHeaders req)
        req' = req { requestHeaders = headers }
        baseUrl = BaseUrl Https burl 443 ""
        man = manager renv
        creq = requestToClientRequestUnchunked baseUrl req'
      trace (show creq) (pure ())
      cresponse <- sendM $ catchConnectionError $ Client.httpLbs creq man
      case cresponse of
        Left err -> throw (fromServantError err)
        Right cresp -> do
          let (status, response) = ( statusCode $ Client.responseStatus cresp
                                   , clientResponseToResponse id cresp
                                   )
          if | status >= 200 && status <= 300 -> pure response
             | otherwise -> throw $ fromRequestStatusResponse creq status response
    go (StreamingRequest req) = undefined -- TODO: Needed to properly support live threads
    go (ThrowClientError e) = throw . fromServantError $ e

