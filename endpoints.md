This file documents what endpoints are currently covered by the wrapper, and where. It is laid out to match the [by section](https://www.reddit.com/dev/api) layout of reddit's own API documentation.

Endpoints marked with [-] are partially implemented, meaning they lack documentation and/or complete models

# [ ] Account (low-ish priority)
  - [ ] GET /api/v1/me
  - [ ] GET /api/v1/me/blocked
  - [ ] GET /api/v1/me/karma
  - [ ] GET /api/v1/me/prefs
  - [ ] GET /api/v1/me/trophies
  - [ ] GET /prefs/blocked
  - [ ] GET /prefs/friends
  - [ ] GET /prefs/messaging
  - [ ] GET /prefs/trusted

# [ ] Captcha
  - [ ] GET /api/needs\_captcha
    - Support not planned; I believe this is obsolete with oauth

# [x] Emoji
  - [x] POST /api/v1/*subreddit*/emoji.json
  - [x] DELETE /api/v1/*subreddit*/emoji/*emoji_name*
  - [x] POST /api/v1/*subreddit*/emoji\_asset\_upload\_s3.json
  - [x] POST /api/v1/*subreddit*/emoji\_custom\_size
  - [x] GET /api/v1/emojis/all

# [x] Flair
  - [x] POST /r/*subreddit*/api/clearflairtemplates
  - [x] POST /r/*subreddit*/api/deleteflair
  - [x] POST /r/*subreddit*/api/deleteflairtemplate
  - [x] POST /r/*subreddit*/api/flair
    - For setting old reddit flairs, as a mod
  - [x] PATCH /r/*subreddit*/api/flair\_template\_order
  - [x] POST /r/*subreddit*/api/flairconfig
  - [x] POST /r/*subreddit*/api/flaircsv
  - [x] GET /r/*subreddit*/api/flairlist
  - [x] POST /r/*subreddit*/api/flairselector
  - [x] POST /r/*subreddit*/api/flairtemplate
  - [x] POST /r/*subreddit*/api/flairtemplate\_v2
  - [x] GET /r/*subreddit*/api/link\_flair
  - [x] GET /r/*subreddit*/api/link\_flair\_v2
  - [x] POST /r/*subreddit*/api/selectflair
    - For settings flairs as a user; text\_color and background\_color do nothing.
  - [x] POST /r/*subreddit*/api/setflairenabled
  - [x] GET /r/*subreddit*/api/user\_flair
  - [x] GET /r/*subreddit*/api/user\_flair\_v2

# [ ] Reddit Gold
  - [ ] POST /api/v1/gold/gild/*fullname*
  - [ ] POST /api/v1/gold/give/*username*

# [ ] Links and Comments
  - [ ] POST /api/comment
  - [ ] POST /api/del
  - [ ] POST /api/editusertext
  - [ ] POST /api/hide
  - [ ] GET /api/info
  - [ ] POST /api/lock
  - [ ] POST /api/marknsfw
  - [ ] GET /api/morechildren
  - [ ] POST /api/report
  - [ ] POST /api/save
  - [ ] GET /api/saved\_categories
  - [ ] POST /api/sendreplies
  - [ ] POST /api/set\_contest\_mode
  - [ ] POST /api/set\_subreddit\_sticky
  - [ ] POST /api/set\_suggested\_sort
  - [ ] POST /api/spoiler
  - [ ] POST /api/store\_visits
    - Support not planned; unsure what this does and it requires premium to test. Someone with premium is welcome to implement it though.
  - [ ] POST /api/submit
  - [ ] POST /api/unhide
  - [ ] POST /api/unlock
  - [ ] POST /api/unmarknsfw
  - [ ] POST /api/unsave
  - [ ] POST /api/unspoiler
  - [ ] POIST /api/vote

# [ ] Listings
  - [ ] GET /api/trending\_subreddits
  - [ ] GET /best
  - [ ] GET /by\_id/*names*
  - [ ] GET /comments/*article*
  - [ ] GET /duplicates/*article*
  - [-] GET [/r/*subreddit*]/hot
  - [-] GET [/r/*subreddit*]/new
  - [ ] GET [/r/*subreddit*]/random
  - [-] GET [/r/*subreddit*]/rising
  - [-] GET [/r/*subreddit*]/top
  - [-] GET [/r/*subreddit*]/controversial

# [ ] Live Threads (lowest priority)
  - [ ] GET /api/live/by\_id/*names*
  - [ ] POST /api/live/create
  - [ ] GET /api/live/happening\_now
  - [ ] POST /api/love/*thread*/accept\_contributor\_invite
  - [ ] POST /api/live/*thread*/close\_thread
  - [ ] POST /api/live/*thread*/delete\_update
  - [ ] POST /api/live/*thread*/edit
  - [ ] POST /api/live/*thread*/hide\_discussion
  - [ ] POST /api/live/*thread*/invite\_contributor
  - [ ] POST /api/live/*thread*/leave\_contributor
  - [ ] POST /api/live/*thread*/report
  - [ ] POST /api/live/*thread*/rm\_contributor
  - [ ] POST /api/live/*thread*/rm\_contributor\_invite
  - [ ] POST /api/live/*thread*/set\_contributor\_permissions
  - [ ] POST /api/live/*thread*/strike\_update
  - [ ] POST /api/live/*thread*/unhide\_discussion
  - [ ] POST /api/live/*thread*/update
  - [ ] GET /live/*thread*
  - [ ] GET /live/*thread*/about
  - [ ] GET /live/*thread*/contributors
  - [ ] GET /live/*thread*/discussions
  - [ ] GET /live/*thread*/updates/*update_id*

# [ ] Private Messages
  - [ ] POST /api/block
  - [ ] POST /api/collapse\_message
  - [ ] POST /api/compose
  - [ ] POST /api/del\_msg
  - [ ] POST /api/read\_all\_messages
  - [ ] POST /api/unblock\_subreddit
  - [ ] POST /api/uncollapse\_message
  - [ ] POST /api/unread\_message
  - [ ] GET /message/*where*

# [ ] Misc
  - [ ] GET /r/*subreddit*/saved\_media\_text
  - [ ] GET /api/v1/scopes

# [ ] Moderation
  - [ ] GET /r/*subreddit*/about/log
  - [-] GET /r/*subreddit*/about/*location*
  - [ ] POST /r/*subreddit*/api/accept\_moderator\_invite
  - [ ] POST /api/approve
  - [ ] POST /api/distinguish
  - [ ] POST /api/ignore\_reports
  - [ ] POST /api/leavecontributor
  - [ ] POST /api/leavemoderator
  - [ ] POST /api/mute\_message\_author
  - [ ] POST /api/remove
  - [ ] POST /api/unignore\_reports
  - [ ] POST /api/unmute\_message\_author
  - [x] GET /r/*subreddit*/stylesheet

# [ ] New Modmail (low-ish priority)
  - [ ] POST /api/mod/bulk\_read
  - [ ] GET /api/mod/conversations
  - [ ] POST /api/mod/conversations
  - [ ] GET /api/mod/conversations/*conversation\_id*
  - [ ] POST /api/mod/conversations/*conversation\_id*
  - [ ] POST /api/mod/conversations/*conversation\_id*/archive
  - [ ] DELETE /api/mod/conversations/*conversation\_id*/highlight
  - [ ] POST /api/mod/conversations/*conversation\_id*/highlight
  - [ ] POST /api/mod/conversations/*conversation\_id*/mute
  - [ ] POST /api/mod/conversations/*conversation\_id*/unarchive
  - [ ] POST /api/mod/conversations/*conversation\_id*/unmute
  - [ ] GET /api/mod/conversations/*conversation\_id*/user
  - [ ] POST /api/mod/conversations/read
  - [ ] GET /api/mod/conversations/subreddits
  - [ ] POST /api/mod/conversations/unread
  - [ ] GET /api/mod/conversations/unread/count

# [ ] Multis (low priority)
  - [ ] POST /api/muli/copy
  - [ ] GET /api/multi/mine
  - [ ] POST /api/multi/rename
  - [ ] GET /api/multi/user/*username*
  - [ ] DELETE /api/multi/*multipath*|*filterpath*
  - [ ] GET /api/multi/*multipath*|*filterpath*
  - [ ] POST /api/multi/*multipath*|*filterpath*
  - [ ] PUT /api/multi/*multipath*|*filterpath*
    - Appears to be identical to the POST version
  - [ ] GET /api/multi/*multipath*/description
  - [ ] DELETE /api/multi/*multipath*|*filterpath*/r/*srname*
  - [ ] GET /api/multi/*multipath*|*filterpath*/r/*srname*
  - [ ] PUT /api/multi/*multipath*|*filterpath*/r/*srname*

# [ ] Search
  - [ ] GET [/r/subreddit]/search

# [ ] Subreddits
  - [-] GET /r/*subreddit*/about/*where*
  - [x] POST /r/*subreddit*/api/delete\_sr\_banner
  - [x] POST /r/*subreddit*/api/delete\_sr\_header
  - [x] POST /r/*subreddit*/api/delete\_sr\_icon
  - [x] POST /r/*subreddit*/api/delete\_sr\_image
  - [ ] GET /api/reccomend/sr/*srnames*
  - [ ] GET /api/search\_reddit\_names
  - [ ] POST /api/search\_reddit\_names
    - Appears to be identical to the GET version
  - [ ] POST /api/search\_subreddits
    - Appears to be identical to /api/search\_reddit\_names
  - [ ] POST /api/site\_admin
  - [x] GET /r/*subreddit*/api/submit\_text
  - [ ] GET /api/subreddit\_autocomplete
  - [ ] GET /api/subreddit\_autocomplete\_v2
    - Will probably only support one of these autocomplete endpoints
  - [x] POST /r/*subreddit*/api/subreddit\_stylesheet
  - [ ] POST /api/subscribe
  - [x] POST /r/*subreddit*/api/upload\_sr\_img
  - [ ] GET /profiles/search
    - Why is this in the subreddits section?
  - [ ] GET /r/*subreddit*/about
  - [x] GET /r/*subreddit*/about/edit
  - [ ] GET /r/*subreddit*/about/rules
  - [ ] GET /r/*subreddit*/about/traffic
  - [ ] GET /r/*subreddit*/sidebar
  - [ ] GET /r/*subreddit*/sticky
  - [ ] GET /subreddits/mine/*where*
  - [ ] GET /subreddits/search
  - [ ] GET /subreddits/*where*
  - [ ] GET /users/where

# [ ] Users
  - [ ] POST /api/block\_user
  - [x] POST /r/*subreddit*/api/friend
    - The actual friend and block portions of this are not implemented, because there exist other endpoints for thoseand they don't require a subreddit context unlike the rest
  - [ ] POST /api/report\_user
  - [x] POST /r/*subreddit*/api/setpermissions
  - [x] POST /r/*subreddit*/api/unfriend
    - The actual friend and block portions of this are not implemented, because there exist other endpoints for thoseand they don't require a subreddit context unlike the rest
  - [ ] GET /api/user\_data\_by\_account\_ids
  - [ ] GET /api/username\_available
  - [ ] DELETE /api/v1/me/friends/*username*
  - [ ] GET /api/v1/me/friends/*username*
  - [ ] PUT /api/v1/me/friends/*username*
  - [ ] GET /api/v1/user/*username*/trophies
  - [ ] GET /user/*username*/*where*

# [ ] Widgets: These probably are supposed to be r/subreddit/api/widget, but it doesn't say that on the docs
  - [ ] POST /api/widget
  - [ ] DELETE /api/widget/*widget_id*
  - [ ] PUT /api/widget/*widget_id*
  - [ ] POST /api/widget\_image\_upload\_s3
  - [ ] PATCH /api/widget\_order/*section*
  - [ ] GET /api/widgets

# [ ] Wiki
  - [ ] POST /r/*subreddit*/api/wiki/alloweditor/*act*
  - [ ] POST /r/*subreddit*/api/wiki/edit
  - [ ] POST /r/*subreddit*/api/wiki/hide
  - [ ] POST /r/*subreddit*/api/wiki/rever
  - [ ] GET /r/*subreddit*/wiki/discussions/*page*
  - [ ] GET /r/*subreddit*/wiki/pages
  - [ ] GET /r/*subreddit*/wiki/revisions
  - [ ] GET /r/*subreddit*/wiki/revisions/*page*
  - [ ] GET /r/*subreddit*/wiki/settings/*page*
  - [ ] POST /r/*subreddit*/wiki/settings/*page*
  - [ ] GET /r/*subreddit*/wiki/*page*
