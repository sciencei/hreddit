{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE ViewPatterns          #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE CPP      #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE AllowAmbiguousTypes      #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE OverloadedLists      #-}
{-# LANGUAGE UndecidableInstances      #-}

module Reddit.Servant.Internal.Misc where

import qualified Reddit.Types.Misc as RT
import Reddit.Types.Emoji (S3Key(..))

import Servant.API ((:>), ToHttpApiData, toQueryParam, Accept(..), MimeRender(..), MimeUnrender(..)
                   , (:<|>), Verb, JSON)
import Data.Aeson.Types (Value)
import Servant.Client.Core ( HasClient(..), appendToQueryString, Request
                           , RequestBody(..), BaseUrl, Scheme(..), RequestF(..)
                           , BaseUrl(..))
import Servant.API.NamedArgs (OptionalNamedParam)
import Servant.Types.SourceT (runSourceT)
import GHC.TypeLits          (Symbol, KnownSymbol, symbolVal)
import Data.Text             (Text, pack)
import Data.Proxy            (Proxy(..))
import Data.String           (fromString)
import Data.Foldable         (toList)
import Data.Maybe            (maybeToList)
import Named                 ((:?), Name(..), argF, (:!), arg)
import Network.HTTP.Client.MultipartFormData (renderPart, PartM)
import Network.HTTP.Media (renderHeader)
import qualified Network.HTTP.Client  as H
import Data.ByteString.Builder (byteString, toLazyByteString)
import Network.HTTP.Types (hContentType, renderQuery)
import qualified Data.ByteString as BS
import qualified Data.Text.Encoding as TE
import qualified Data.ByteString.Lazy as BL
import Network.HTTP.Media.MediaType ((//))
import Data.Bifunctor (first)
import Text.HTML.TagSoup (parseTags, Tag(TagText), isTagCloseName, isTagOpenName)
import Servant.Multipart (MultipartForm(..), MultipartData(..), Tmp)
import Control.Monad.Except (runExceptT)
import Servant.API.Generic ( (:-), GenericMode(..)
                           )

-- | Combinator for a parameter with a constant value
data SuppliedParam (name :: Symbol) (stringValue :: Symbol)

instance (KnownSymbol name, KnownSymbol value, HasClient m api)
    => HasClient m (SuppliedParam name value :> api) where

  type Client m (SuppliedParam name value :> api) = Client m api

  clientWithRoute pm _ req =
      clientWithRoute pm (Proxy :: Proxy api) $ add $
        symbolVal (Proxy :: Proxy value)

    where
      add :: (ToHttpApiData a) => a -> Request
      add param = appendToQueryString pname (Just $ toQueryParam param) req

      pname :: Text
      pname  = pack $ symbolVal (Proxy :: Proxy name)

  hoistClientMonad pm _ f cl =
      hoistClientMonad pm (Proxy :: Proxy api) f cl

-- | Combinator for two parameters with different names, where only one can
-- be supplied. Uses 'bothName' for the name of the named parameter to
-- supply it and then expects an 'Either a b'
-- TODO: Remove or replace, probably
data NamedEitherParam (bothName :: Symbol) (aName :: Symbol) (a :: *) (bName :: Symbol) (b :: *)

-- | Combinator representing one of "before" or "after" in a listing. Less
-- than ideal, but works for now
data Index (a :: RT.ThingType)

instance (HasClient m api, ToHttpApiData (RT.Thing a))
    => HasClient m (Index a :> api) where

  type Client m (Index a :> api) =
    ("index" :? RT.Index a) -> Client m api

  clientWithRoute pm _ req (argF (Name @"index") -> ix) =
      clientWithRoute pm (Proxy :: Proxy api) $
        case ix of
          Nothing -> req
          (Just (RT.Before i)) -> add "before" i
          (Just (RT.After i))  -> add "after" i

    where
      add :: (ToHttpApiData t) => Text -> t -> Request
      add name param = appendToQueryString name (Just $ toQueryParam param) req

  hoistClientMonad pm _ f cl = \a ->
      hoistClientMonad pm (Proxy :: Proxy api) f (cl a)

-- | Converts an HTTP Client request body to a servant version
coerceBody :: H.RequestBody -> RequestBody
coerceBody = undefined
--coerceBody (H.RequestBodyLBS b)           = RequestBodyLBS b
--coerceBody (H.RequestBodyBS b)            = RequestBodyBS b
--coerceBody (H.RequestBodyBuilder i b)     = RequestBodyBuilder i b
--coerceBody (H.RequestBodyStream i p)      = RequestBodyStream i p
--coerceBody (H.RequestBodyStreamChunked p) = RequestBodyStreamChunked p
--coerceBody (H.RequestBodyIO rb)           = RequestBodyIO (coerceBody <$> rb)

-- | Combinator for file uploads using multipart\/form-data
data File (name :: Symbol)

instance (KnownSymbol name, HasClient m api)
    => HasClient m (File name :> api) where

  type Client m (File name :> api) = (name :! RT.EncodedFile) -> Client m api

  clientWithRoute pm _ req (arg (Name @name) -> ef) =
      clientWithRoute pm (Proxy @(MultipartForm Tmp (MultipartData Tmp) :> api)) req (boundary, form)
    where
      boundary = RT.boundary ef
      form = MultipartData [] [RT.encodedFileToFileData "file" ef]

  hoistClientMonad pm _ f cl = \a ->
      hoistClientMonad pm (Proxy @api) f (cl a)

-- | renderParts from Network.HTTP.Client, but without the trailing
-- \r\n (optional as per the RFC and causes reddit to have an internal
-- server error)
renderPartsNoTrailing :: Applicative m
                      => BS.ByteString    -- ^ Boundary between parts.
                      -> [PartM m] -> m H.RequestBody
renderPartsNoTrailing boundary parts = (fin . mconcat) <$> traverse (renderPart boundary) parts
  where fin = (<> cp "--" <> cp boundary <> cp "--")
        cp bs = H.RequestBodyBuilder (fromIntegral $ BS.length bs) $ byteString bs

-- | CSS content type (used for stylesheet endpoint)
data CSS

instance Accept CSS where
    contentType _ = "text" // "css"

instance MimeRender CSS Text where
    mimeRender _ = BL.fromStrict . TE.encodeUtf8

instance MimeUnrender CSS Text where
    mimeUnrender _ = first show . TE.decodeUtf8' . BL.toStrict

-- | XML content type for the S3 endpoint
data XML

instance Accept XML where
  contentTypes _ = ["application" // "xml", "text" // "xml" ]

instance MimeUnrender XML S3Key where
  mimeUnrender _ bs = case takeWhile eKey . dropWhile sKey . parseTags $ bs of
                        [_, TagText t] -> Right . S3Key . TE.decodeUtf8 . BL.toStrict $ t
                        _ -> Left "failed to parse s3 key from response"
                  where
                    sKey = not . isTagOpenName "Key"
                    eKey = not . isTagCloseName "Key"

-- | Helper to make using type synonyms in servant endpoints more convenient
type (f :: j -> k) $> (a :: j) = f a
infixr 4 $>

-- | Options common to all endpoints taking listings
type ListingOptions thingtype next
  =  Index thingtype
  :> OptionalNamedParam "limit"  Int 
  :> OptionalNamedParam "count"  Int 
  :> OptionalNamedParam "show"   RT.ShowAll
  :> next

-- | Turns a servant request into a client request; mostly copied
-- directly from Servant.Client, but with the RequestBody for SourceIO
-- changed to a RequestBodyLBS; this means it loads the entire stream into
-- memory at once.
requestToClientRequestUnchunked :: BaseUrl -> Request -> H.Request
requestToClientRequestUnchunked burl r = H.defaultRequest
    { H.method = requestMethod r
    , H.host = fromString $ baseUrlHost burl
    , H.port = baseUrlPort burl
    , H.path = BL.toStrict
                  $ fromString (baseUrlPath burl)
                 <> toLazyByteString (requestPath r)
    , H.queryString = renderQuery True . toList $ requestQueryString r
    , H.requestHeaders =
      maybeToList acceptHdr ++ maybeToList contentTypeHdr ++ headers
    , H.requestBody = body
    , H.secure = isSecure
    }
  where
    -- Content-Type and Accept are specified by requestBody and requestAccept
    headers = filter (\(h, _) -> h /= "Accept" && h /= "Content-Type") $
        toList $requestHeaders r

    acceptHdr
        | null hs   = Nothing
        | otherwise = Just ("Accept", renderHeader hs)
      where
        hs = toList $ requestAccept r

    convertBody bd = case bd of
        RequestBodyLBS body'       -> H.RequestBodyLBS body'
        RequestBodyBS body'        -> H.RequestBodyBS body'
        RequestBodySource sourceIO -> H.RequestBodyIO $ H.RequestBodyLBS <$> newBody
          where
            newBody = do
              stream <- (runExceptT $ runSourceT sourceIO)
              case stream of
                Left err  -> fail err
                Right bss -> pure . mconcat $ bss

    (body, contentTypeHdr) = case requestBody r of
        Nothing           -> (H.RequestBodyBS "", Nothing)
        Just (body', typ) -> (convertBody body', Just (hContentType, renderHeader typ))

    isSecure = case baseUrlScheme burl of
        Http -> False
        Https -> True

data AsRaw (a :: k)

data AsId (a :: k)

type family MkRaw (route :: k) :: k where
  MkRaw (a :<|> b) = MkRaw a :<|> MkRaw b
  MkRaw (a :> b) = MkRaw a :> MkRaw b
  MkRaw (Verb m s '[JSON] a) = Verb m s '[JSON] Value
  MkRaw a = a

instance (GenericMode mode) => GenericMode (AsId mode) where
    type AsId mode :- api = mode :- api

instance (GenericMode mode) => GenericMode (AsRaw mode) where
    type AsRaw mode :- api = mode :- MkRaw api
