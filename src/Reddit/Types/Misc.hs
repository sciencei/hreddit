{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ViewPatterns #-}

module Reddit.Types.Misc
  ( Listing(..)
  , ImageResponse(..)
  , Thing
  , mkThing
  , IsThingType(..)
  , ThingType(..)
  , TimeInterval(..)
  , UTCPosix(..)
  , SuccessMessage(..)
  , CssClass(..)
  , aesonDecodeOptions
  , emptyTextAsNothing
  , mkColor
  , Color
  , unColor
  , Index(..)
  , ShowAll(..)
  , EncodedFile
  , path
  , boundary
  , encodeFile
  , encodedFileToFileData
  , ImageType(..)
  , ImageUploadType(..)
  ) where

import GHC.Generics (Generic)
import Data.Aeson (Options(..), defaultOptions, FromJSON(..), ToJSON(..), withObject, (.:), (.:?)
                  , withScientific, SumEncoding(..))
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Time.Clock (UTCTime)
import Data.Scientific (toBoundedInteger)
import qualified Data.Text as T
import Data.Text (Text)
import Web.HttpApiData (ToHttpApiData(..))
import Data.Char (toLower, isUpper, isHexDigit)
import Data.Coerce (coerce)
import Data.String (IsString)
import qualified Data.ByteString.Lazy as BL
import Servant.Multipart (genBoundary, FileData(..), Tmp)

data Listing a = Listing
  { after :: Maybe (Thing (AsThingType a))
  , before :: Maybe (Thing (AsThingType a))
  , contents :: [a]
  } deriving (Show, Read, Generic)

data Index a = Before (Thing a)
             | After (Thing a)
             deriving (Show, Read, Eq, Generic)

data ShowAll = ShowAll
  deriving (Show, Read, Eq, Generic)

instance ToHttpApiData ShowAll where
  toQueryParam _ = "all"

data ImageResponse
    = ImageResponse { errors :: [Text]
                    , img_src :: Maybe Text
                    , errors_values :: [Text]
                    } deriving (Show, Read, Generic)

instance FromJSON ImageResponse

instance Foldable Listing where
    foldr f init listing = foldr f init (contents listing)

instance (FromJSON a) => FromJSON (Listing a) where
    parseJSON = withObject "Listing" $ \o -> do
      dat <- o .: "data"
      Listing
        <$> dat .:? "after"
        <*> dat .:? "before"
        <*> dat .: "children"

newtype Thing (a :: ThingType) = Thing Text
  deriving (Show, Read, Eq, Generic, ToJSON, FromJSON, ToHttpApiData)

mkThing :: forall a. (IsThingType a) => Text -> Maybe (Thing (AsThingType a))
mkThing s = if (validateThing @a s)
              then Just (Thing s)
              else Nothing

newtype UTCPosix = UTCPosix { unPosix :: UTCTime } deriving (Show, Read, Eq, Ord)

instance FromJSON UTCPosix where
    parseJSON = withScientific "UTCPosix" $ \s ->
        case toBoundedInteger s of
          Nothing -> undefined
          Just i  -> pure . UTCPosix . posixSecondsToUTCTime $ fromIntegral (i :: Int)

data TimeInterval = Hour
                  | Day
                  | Week
                  | Month
                  | Year
                  | All
                  deriving (Show, Read, Eq, Generic)

instance ToHttpApiData TimeInterval where
    toUrlPiece = T.toLower . T.pack . show

data ThingType = Post'
               | User'
               | Comment'
               | Subreddit'
               deriving (Show, Read, Eq, Generic)

class IsThingType a where
    type AsThingType a :: ThingType
    validateThing :: Text -> Bool

newtype SuccessMessage = SuccessMessage Bool
  deriving (Show, Read, Eq, Generic)

instance FromJSON SuccessMessage where
    parseJSON = withObject "SuccessMessage" $ \o -> SuccessMessage <$> o .: "success"

newtype CssClass = Css { unCss :: Text }
                 deriving (Show, Eq, Generic, Read, ToJSON, ToHttpApiData, FromJSON, IsString)

newtype Color = Color Text
                deriving (Show, Eq, Generic, Read, ToJSON, ToHttpApiData, FromJSON)

mkColor :: Text -> Maybe Color
mkColor t@(T.uncons -> Just ('#', rest))
  | T.length rest == 6 && (T.all isHexDigit rest) = Just . Color $ t
mkColor _ = Nothing

unColor :: Color -> Text
unColor = coerce

aesonDecodeOptions :: Options
aesonDecodeOptions = defaultOptions { fieldLabelModifier = toSnake
                                    , constructorTagModifier = map toLower
                                    , allNullaryToStringTag = True
                                    , omitNothingFields = False
                                    , sumEncoding = UntaggedValue
                                    , tagSingleConstructors = True
                                    }
  where
    lowerCaseFst (x:xs) = (toLower x):xs
    lowerCaseFst [] = []
    toSnake = foldr (\x acc -> if isUpper x then '_':(toLower x):acc else x:acc) [] . lowerCaseFst

emptyTextAsNothing :: (Monad m) => Maybe Text -> m (Maybe Text)
emptyTextAsNothing t = pure $ t >>= \t' -> if T.null t' then Nothing else Just t'

-- | A filepath combined with a generated boundary to be used for
-- uploading. Construct one from a filepath with 'encodeFile'. Note that
-- this does not actually load the file into memory or touch the filesystem in any way.
data EncodedFile = EncodedFile { _path     :: FilePath -- ^ The path to the file
                               , _boundary :: BL.ByteString -- ^ Boundary for the file, to be used in the form
                               } deriving (Generic)

path :: EncodedFile -> FilePath
path = _path

boundary :: EncodedFile -> BL.ByteString
boundary = _boundary

-- | Constructs an 'EncodedFile' with a randomly generated boundary
-- (mostly copied from 'Network.HTTP.Client.Request.observedStreamFile')
encodeFile :: FilePath -> IO EncodedFile
encodeFile f = do
    b <- genBoundary
    pure $ EncodedFile f b

-- | Converts an 'EncodedFile' to a 'FileData' for use in 'MultipartData'
encodedFileToFileData :: Text -> EncodedFile -> FileData Tmp
encodedFileToFileData name (EncodedFile p _) = FileData name (T.pack p) "application/octet-stream" p

-- | Filetype of an image being uploaded.
data ImageType = PNG | JPG
               deriving (Show, Read, Eq, Generic)

instance ToHttpApiData ImageType where
    toQueryParam = T.toLower . T.pack . show

-- | Role an image plays on a subreddit.
data ImageUploadType = Header | Icon | Banner
               deriving (Show, Read, Eq, Generic)

instance ToHttpApiData ImageUploadType where
    toQueryParam = T.toLower . T.pack . show

