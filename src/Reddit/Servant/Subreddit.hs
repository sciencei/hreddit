{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ConstraintKinds #-}

module Reddit.Servant.Subreddit where

import Reddit.Servant.Internal.Misc
import Reddit.Types.Misc hiding (Index(..))
import Reddit.Types.User
import Reddit.Types.Subreddit
import qualified Reddit.Types.Post as P

import GHC.Generics (Generic)
import Servant.API ((:>), Post, Get, JSON, NoContent)
import Servant.API.Generic (ToServant, (:-), toServant, fromServant, AsApi)
import Servant.API.NamedArgs (RequiredNamedParam, OptionalNamedParam)
import Data.Text (Text)
import Servant.Client.Core (HasClient(..), RunClient(..))
import Servant.Client.Generic (AsClientT)
import Data.Proxy (Proxy(..))
import Data.Aeson (Value)
import Data.Set (Set)

import Reddit.Servant.Internal.Misc (AsId, AsRaw, MkRaw)

-- |Routes that take a subreddit. Called through
-- 'Reddit.Servant.Reddit.RedditApi', all these endpoints have
-- "\/r\/{subreddit}" appended to the beginning
data SubSpecificApi route = SubSpecificApi 
  { banned :: route :- "about" :> "banned" :> ListingOptions User' $> Get '[JSON] (Listing BanEntry)
  , muted :: route :- "about" :> "muted" :> ListingOptions User' $> Get '[JSON] (Listing RoleEntry)
  , wikiBanned :: route :- "about" :> "wikibanned" :> ListingOptions User' $>  Get '[JSON] (Listing BanEntry)
  , contributors :: route :- "about" :> "contributors" :> ListingOptions User' $>  Get '[JSON] (Listing RoleEntry)
  -- | #wikiContributors#
  --
  --   [@Endpoint@]
  --       < GET /about/moderators>
  --   [@Description@]
  --       Gets the wiki contributors for the subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --
  --       [@index :: 'Reddit.Types.Misc.Index' 'User''@] An anchor item to only return results
  --       before or after a given 'Thing'.
  --       [@limit :: 'Int'@] Maximum number of items to return; defaults
  --       to and maxes out at 100
  --       [@count :: 'Int'@] The number of items already seen in this listing; affects
  --       the before and after fields in the response
  --       [@show :: 'ShowAll'@] If passed 'ShowAll', disables filters on the listing 
  --       like "hide links that I have voted on"
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  , wikiContributors :: route :- "about" :> "wikicontributors" :> ListingOptions User' $> Get '[JSON] (Listing RoleEntry)
  -- | #moderators#
  --
  --   [@Endpoint@]
  --       < GET /about/moderators>
  --   [@Description@]
  --       Gets the moderators for the subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --
  --       [@index :: 'Reddit.Types.Misc.Index' 'User''@] An anchor item to only return results
  --       before or after a given 'Thing'.
  --       [@limit :: 'Int'@] Maximum number of items to return; defaults
  --       to and maxes out at 100
  --       [@count :: 'Int'@] The number of items already seen in this listing; affects
  --       the before and after fields in the response
  --       [@show :: 'ShowAll'@] If passed 'ShowAll', disables filters on the listing 
  --       like "hide links that I have voted on"
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  , moderators :: route :- "about" :> "moderators" :> ListingOptions User' $> Get '[JSON] (Listing Mod)
  , new :: route :- "new" :> SubredditSort
  , hot :: route :- "hot" :> SubredditSort
  , rising :: route :- "rising" :> SubredditSort
  , best :: route :- "best" :> SubredditSort
  , top :: route :- "top" :> SubredditSort
  , controversial :: route :- "controversial" :> SubredditSort
  -- | #deleteMobileBanner#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_delete_sr_banner POST /api/delete_sr_banner>
  --   [@Description@]
  --       Deletes the mobile banner
  --       image for a subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --      See 'deleteImage', 'deleteHeader', and
  --      'deleteIcon' to delete other subreddit images.
  --      See 'uploadImage' to upload a subreddit mobile banner. Throws
  --      'Reddit.Types.Error.Forbidden'
  --      if used on a subreddit without the right permissions.
  , deleteMobileBanner :: route :- "api" :> "delete_sr_banner" :> Post '[JSON] SuccessMessage
  -- | #deleteHeader#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_delete_sr_header POST /api/delete_sr_header>
  --   [@Description@]
  --       Deletes the header (ie, the image in the top left on old reddit)
  --       image for a subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --      See 'deleteImage', 'deleteMobileBanner', and
  --      'deleteIcon' to delete other subreddit images.
  --      See 'uploadImage' to upload a subreddit header. Throws
  --      'Reddit.Types.Error.Forbidden'
  --      if used on a subreddit without the right permissions.
  , deleteHeader :: route :- "api" :> "delete_sr_header" :> Post '[JSON] SuccessMessage
  -- | #deleteIcon#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_delete_sr_icon POST /api/delete_sr_icon>
  --   [@Description@]
  --       Deletes the custom mobile icon for a subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --      See 'deleteImage', 'deleteMobileBanner', and
  --      'deleteHeader' to delete other subreddit images.
  --      See 'uploadImage' to upload a subreddit icon. Throws
  --      'Reddit.Types.Error.Forbidden'
  --      if used on a subreddit without the right permissions.
  , deleteIcon :: route :- "api" :> "delete_sr_icon" :> Post '[JSON] SuccessMessage
  -- | #deleteImage#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_delete_sr_img POST /api/delete_sr_img>
  --   [@Description@]
  --       Deletes an image from the subreddit's custom image set (the
  --       stylesheet images).
  --   [@Required Parameters@]
  --
  --       [@img_name :: 'Text'@] Name of the image to delete
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --      See 'deleteIcon', 'deleteMobileBanner', and
  --      'deleteHeader' to delete other subreddit images.
  --      See 'uploadCustomImage' to upload subreddit images. This
  --      function does /not/ deal with redesign images. Throws
  --      'Reddit.Types.Error.Forbidden'
  --      if used on a subreddit without the right permissions.
  , deleteImage :: route :- "api" :> "delete_sr_img" 
                                  :> RequiredNamedParam "img_name" Text
                                  :> Post '[JSON] SuccessMessage
  -- | #uploadImage#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_upload_sr_img POST /api/upload_sr_img>
  --   [@Description@]
  --       Add or replace a subreddit header, icon, or mobile banner.
  --   [@Required Parameters@]
  --
  --       [@file :: 'EncodedFile'@] The image to upload
  --       [@upload_type :: 'ImageUploadType'@] Type of image being added
  --       or replaced
  --
  --   [@Optional Parameters@]
  --
  --       [@img_type :: 'ImageType'@] The type of the actual image file
  --       (png or jpg)
  --
  --   [@Returns@]
  --       'ImageResponse'
  --   [@Notes@]
  --      Throws 413 Payload Too Large if the image is too big (> 500kb).
  --      Throws 'Reddit.Types.Error.Forbidden' if used on a subreddit
  --      without the right permissions. See 'deleteIcon',
  --      'deleteMobileBanner', and 'deleteHeader' to delete these images.
  --      See 'uploadCustomImage' to upload an image to the subreddit's
  --      stylsheet page.
  , uploadImage :: route :- "api" :> "upload_sr_img"
                                  :> OptionalNamedParam "img_type" ImageType
                                  :> RequiredNamedParam "upload_type" ImageUploadType
                                  :> File "file"
                                  :> Post '[JSON] ImageResponse
  -- | #uploadCustomImage#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_upload_sr_img POST /api/upload_sr_img?upload_type=img>
  --   [@Description@]
  --       Add or replace a subreddit stylesheet image.
  --   [@Required Parameters@]
  --
  --       [@file :: 'EncodedFile'@] The image to upload
  --       [@name :: 'Text'@] The name of the image once uploaded
  --
  --   [@Optional Parameters@]
  --
  --       [@img_type :: 'ImageType'@] The type of the actual image file
  --       (png or jpg)
  --
  --   [@Returns@]
  --       'ImageResponse'
  --   [@Notes@]
  --      Throws 413 Payload Too Large if the image is too big (> 500kb).
  --      Throws 'Reddit.Types.Error.Forbidden' if used on a subreddit
  --      without the right permissions. See 'deleteImage,
  --      to delete these images.
  --      See 'uploadImage' to upload a header, mobile banner, or icon.
  , uploadCustomImage :: route :- "api" :> "upload_sr_img"
                                  :> SuppliedParam "upload_type" "img"
                                  :> OptionalNamedParam "img_type" ImageType
                                  :> RequiredNamedParam "name" Text
                                  :> File "file"
                                  :> Post '[JSON] ImageResponse
  -- | #submitText#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_api_submit_text GET /api/submit_text>
  --   [@Description@]
  --       Gets the submit text for a subreddit
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SubmitText'
  --   [@Notes@]
  , submitText :: route :- "api" :> "submit_text" :> Get '[JSON] SubmitText
  -- | #setStylesheet#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_subreddit_stylesheet POST /api/subreddit_stylesheet?op=save>
  --   [@Description@]
  --       Sets the stylesheet for the subreddit.
  --   [@Required Parameters@]
  --
  --       [@stylesheet_contents :: 'Text'@] The new text of the stylesheet
  --       [@reason :: 'Text'@] A reason to display in the mod
  --       log/stylesheet history
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       See 'stylesheet' to retrieve the current stylesheet contents.
  --       Throws 'Reddit.Types.Error.Forbidden' on a subreddit without the
  --       proper permissions, and fails with a 'False' 'SuccessMessage' if
  --       the supplied new stylsheet is invalid Reddit CSS.
  , setStylesheet :: route :- "api" :> "subreddit_stylesheet" :> SuppliedParam "op" "save"
                                    :> RequiredNamedParam "reason" Text 
                                    :> RequiredNamedParam "stylesheet_contents" Text
                                    :> Post '[JSON] SuccessMessage
  -- | #stylesheet#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_stylesheet GET /stylesheet>
  --   [@Description@]
  --       Gets the stylesheet for the subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'Text'
  --   [@Notes@]
  --       Throws 'Reddit.Types.Error.NotFound' if the subreddit doesn't
  --       have a stylesheet. See 'setStylesheet' to set the stylesheet to
  --       something else.
  , stylesheet :: route :- "stylesheet" :> Get '[CSS] Text
  -- |Gets settings; returns a 404 rather than a 403 if you don't have
  -- permissions to view (but are logged in), and returns an empty listing
  -- (that will cause a parse error) if used on a subreddit that doesn't
  -- exist. The documentation includes a created (true or false) parameter,
  -- but it doesn't seem to do anything so it's been left out here
  -- | #settings#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_r_{subreddit}_about_edit GET /about/edit>
  --   [@Description@]
  --       Returns the settings for the subreddit.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SubredditSettings'
  --   [@Notes@]
  --       For some reason throws a 'Reddit.Types.Error.NotFound' if logged in, but without
  --       permission to view the subreddit's settings. The documentation
  --       includes a @created@ parameter, but testing didn't reveal any
  --       changes upon setting or not setting it, so it has been ommitted
  --       here.
  , settings :: route :- "about" :> "edit" :> Get '[JSON] SubredditSettings
  -- | #ban#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_friend POST /api/friend?type=banned>
  --   [@Description@]
  --       Bans a user from the subreddit.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user to ban
  --
  --   [@Optional Parameters@]
  --
  --       [@note :: 'Text'@] Information about the ban; this is mostly
  --       interchangeable with @ban_message@ and the two are concatenated
  --       together with a @'@ on the actual site.
  --       [@duration :: 'Int'@] Length of the ban in days; defaults to
  --       permanent
  --       [@ban_message :: 'Text'@] Message sent to the user regarding the
  --       ban
  --       [@ban_reason :: 'Text'@] Reason for the ban
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have banning permissions. This can be used on a user who
  --       is already banned, and replaces the current ban duration with
  --       the new one. See 'unban' for the reverse.
  , ban :: route :- "api" :> "friend" 
                          :> SuppliedParam "type" "banned"
                          :> OptionalNamedParam "ban_reason" Text
                          :> OptionalNamedParam "ban_message" Text
                          :> RequiredNamedParam "name" User
                          :> OptionalNamedParam "duration" Int
                          :> OptionalNamedParam "note" Text
                          :> Post '[JSON] SuccessMessage
  -- | #unban#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_unfriend POST /api/unfriend?type=banned>
  --   [@Description@]
  --       Unbans a user from the subreddit.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user to unban
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'NoContent'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have banning permissions. This can be used on a user who
  --       is already not banned, to no effect. See 'ban' for the reverse.
  , unban :: route :- "api" :> "unfriend"
                            :> SuppliedParam "type" "banned"
                            :> RequiredNamedParam "name" User
                            :> Post '[NoContent] NoContent
  -- | #wikiBan#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_friend POST /api/friend?type=wikibanned>
  --   [@Description@]
  --       Bans a user from the subreddit's wiki
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user to ban
  --
  --   [@Optional Parameters@]
  --
  --       [@duration :: 'Int'@] Length of the ban in days (defaults to
  --       permanent)
  --       [@note :: 'Text'@] Note about the ban
  --
  --   [@Returns@]
  --       'NoContent'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have wikibanning permissions. See 'wikiUnban' for the
  --       opposite.
  , wikiBan :: route :- "api" :> "friend"
                              :> SuppliedParam "type" "wikibanned"
                              :> RequiredNamedParam "name" User
                              :> OptionalNamedParam "note" Text
                              :> OptionalNamedParam "duration" Int
                              :> Post '[NoContent] NoContent
  -- | #wikiUnban#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_unfriend POST /api/unfriend?type=wikibanned>
  --   [@Description@]
  --       Unbans a user from the subreddit's wiki.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user to unban
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'NoContent'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have wikibanning permissions. See 'wikiBan' for the
  --       opposite.
  , wikiUnban :: route :- "api" :> "unfriend" 
                          :> SuppliedParam "type" "wikibanned"
                          :> RequiredNamedParam "name" User
                          :> Post '[NoContent] NoContent
  -- |Gives the user given by "name" the role given by "type"
  -- | #addRole#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_friend POST /api/friend>
  --   [@Description@]
  --       Gives a user a 'Role' on the subreddit.
  --   [@Required Parameters@]
  --
  --       [@type :: 'Role'@] What kind of role to give the user
  --       [@name :: 'User'@] The user to give the role to
  --
  --   [@Optional Parameters@]
  --
  --       [@note :: 'Text'@] Note to be shown in the modlog
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions. See 'removeRole' for the
  --       opposite.
  , addRole :: route :- "api" :> "friend"
                              :> RequiredNamedParam "name" User
                              :> RequiredNamedParam "type" Role
                              :> OptionalNamedParam "note" Text
                              :> Post '[JSON] SuccessMessage
  -- | #addRole#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_unfriend POST /api/unfriend>
  --   [@Description@]
  --       Removes a user's 'Role' on the subreddit.
  --   [@Required Parameters@]
  --
  --       [@type :: 'Role'@] What kind of role to remove from the user
  --       [@name :: 'User'@] The user to give the role to
  --
  --   [@Optional Parameters@]
  --
  --       [@note :: 'Text'@] Note to be shown in the modlog
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions. See 'addRole' for the
  --       opposite. Will do nothing (but not error) if the user doesn't
  --       have the given role.
  , removeRole :: route :- "api" :> "unfriend"
                                 :> RequiredNamedParam "name" User
                                 :> RequiredNamedParam "type" Role
                                 :> Post '[NoContent] NoContent
  -- | #inviteMod#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_friend POST /api/friend?type=moderator_invite>
  --   [@Description@]
  --       Invite a user to moderate the subreddit.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user to invite.
  --
  --   [@Optional Parameters@]
  --
  --       [@permissions :: 'Set' 'ModPermission'@] The permissions to give
  --       the new mod. Defaults to 'Reddit.Types.User.All'
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions to add mods. See 'removeMod'
  --       for the opposite.
  , inviteMod :: route :- "api" :> "friend"
                                :> SuppliedParam "type" "moderator_invite"
                                :> RequiredNamedParam "name" User
                                :> OptionalNamedParam "permissions" (Set ModPermission)
                                :> Post '[JSON] SuccessMessage
  -- | #removeMod#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_unfriend POST /api/unfriend?type=moderator>
  --   [@Description@]
  --       Remove a moderator from the subreddit.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user to remove as moderator
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'NoContent'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions to add mods, OR if the user
  --       given is not a moderator. See 'addMod'
  --       for the opposite.
  , removeMod :: route :- "api" :> "unfriend"
                                :> SuppliedParam "type" "moderator"
                                :> RequiredNamedParam "name" User
                                :> Post '[NoContent] NoContent
  -- | #setModPermissions#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_setpermissions POST /api/setpermissions?type=moderator>
  --   [@Description@]
  --       Sets the permissions for a mod on the subreddit.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The mod whose permissions will be set.
  --
  --   [@Optional Parameters@]
  --
  --       [@permissions :: 'Set' 'ModPermission'@] New permissions for the
  --       mod. Defaults to 'Reddit.Types.User.All'
  --
  --   [@Returns@]
  --       'Value'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions, OR if the user
  --       given is not a moderator. See 'addMod'
  --       to add a new mod entirely.
  , setModPermissions :: route :- "api" :> "setpermissions"
                                        :> SuppliedParam "type" "moderator"
                                        :> RequiredNamedParam "name" User
                                        :> OptionalNamedParam "permissions" (Set ModPermission)
                                        :> Post '[JSON] Value
  } deriving (Generic)

instance (RunClient m, HasClient m (ToServant SubSpecificApi AsApi)) => HasClient m (SubSpecificApi (AsId as)) where
    type Client m (SubSpecificApi (AsId as)) = SubSpecificApi (AsClientT m)
    clientWithRoute m _ = fromServant . clientWithRoute m (Proxy @(ToServant SubSpecificApi AsApi))
    hoistClientMonad m _ nt = fromServant . hoistClientMonad m (Proxy @(ToServant SubSpecificApi AsApi)) nt . toServant

instance (RunClient m, HasClient m (MkRaw (ToServant SubSpecificApi AsApi)))
         => HasClient m (SubSpecificApi (AsRaw as)) where
    type Client m (SubSpecificApi (AsRaw as)) = SubSpecificApi (AsRaw (AsClientT m))
    clientWithRoute m _ = fromServant . clientWithRoute m (Proxy @(MkRaw (ToServant SubSpecificApi AsApi)))
    hoistClientMonad m _ nt = fromServant . hoistClientMonad m (Proxy @(MkRaw (ToServant SubSpecificApi AsApi))) nt . toServant

-- | Options common to all subreddit sorts
type SubredditSort
  = OptionalNamedParam "t" TimeInterval
  :> OptionalNamedParam "include_categories" Bool
  :> OptionalNamedParam "sr_detail" Bool
  :> ListingOptions Post'
  $> Get '[JSON] (Listing P.Post)
