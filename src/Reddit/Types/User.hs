{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE TypeFamilies          #-}

module Reddit.Types.User where

import Data.Aeson
import Data.Text (Text)
import GHC.Generics (Generic)
import Reddit.Types.Misc hiding (TimeInterval(..))
import Web.HttpApiData (ToHttpApiData(..))
import qualified Data.Text as T
import Data.Char (toLower, isUpper)
import Data.Set (toList, Set)

newtype FlairClass = FlairClass { getClass :: Text }
  deriving (Show, Read, Eq, Generic, FromJSON, ToJSON)

data RoleEntry = RoleEntry
  { id :: Thing User'
  , name :: User
  , date :: UTCPosix
  } deriving (Show, Read, Eq, Generic)

instance FromJSON RoleEntry

data Role = Contributor
          | Muted
          | WikiContributor
          deriving (Show, Read, Eq, Generic)

instance ToHttpApiData Role where
    toQueryParam t = T.toLower . T.pack . show $ t

newtype User = U { unUser :: Text }
              deriving (Show, Read, Eq, Generic, ToHttpApiData, FromJSON, ToJSON)

instance IsThingType User where
  type AsThingType User = User'
  validateThing = T.isPrefixOf "t2_"

data BanEntry = BanEntry
  { id :: Thing User'
  , note :: Text
  , name :: User
  , date :: UTCPosix
  } deriving (Show, Read, Eq, Generic)

instance FromJSON BanEntry where
    parseJSON = genericParseJSON defaultOptions { unwrapUnaryRecords = True }

data Mod = Mod
  { flairClass :: Maybe FlairClass
  , flairText  :: Maybe Text
  , date       :: UTCPosix
  , id         :: Thing User'
  , name       :: User
  , permissions :: [ModPermission]
  } deriving (Show, Read, Eq, Generic)

instance FromJSON Mod where
    parseJSON = withObject "Mod" $ \o -> Mod
      <$> o .: "author_flair_css_class"
      <*> o .: "author_flair_text"
      <*> o .: "date"
      <*> o .: "id"
      <*> o .: "name"
      <*> o .: "mod_permissions"

data ModPermission = All
                   | ChatConfig
                   | ChatOperator
                   | Posts
                   | Access
                   | Mail
                   | Config
                   | Flair
                   | Wiki
                   deriving (Show, Read, Eq, Generic, Ord)

instance ToHttpApiData (Set ModPermission) where
    toQueryParam = T.intercalate "," . fmap (T.pack . ('+':) . toSnake . show) . toList
      where 
            toSnake = foldr (\x acc -> if isUpper x then '_':(toLower x):acc else x:acc) [] . lowerCaseFst
            lowerCaseFst (x:xs) = (toLower x):xs
            lowerCaseFst [] = []

instance FromJSON ModPermission where
    parseJSON = withText "ModPermission" $ \t -> return $
      case t of
        "all"           -> All
        "chat_config"   -> ChatConfig
        "chat_operator" -> ChatOperator
        "posts"         -> Posts
        "access"        -> Access
        "mail"          -> Mail
        "config"        -> Config
        "flair"         -> Flair
        "wiki"          -> Wiki
