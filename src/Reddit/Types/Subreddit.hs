{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RecordWildCards #-}

module Reddit.Types.Subreddit where

import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Data.Aeson (FromJSON(..), withText, withObject, (.:), (.:?), genericParseJSON)
import Web.HttpApiData (ToHttpApiData(..))
import Reddit.Types.Misc (IsThingType(..), ThingType(..), Thing, aesonDecodeOptions, emptyTextAsNothing)

data SubmitText = SubmitText
  { submitText     :: Maybe Text
  , submitTextHtml :: Maybe Text
  } deriving (Show, Read, Eq, Generic)

instance FromJSON SubmitText where
    parseJSON = withObject "SubmitText" $ \o -> do
      submitText     <- o .:? "submit_text" >>= emptyTextAsNothing
      submitTextHtml <- o .:? "submit_text_html" >>= emptyTextAsNothing
      pure SubmitText{..}

data CommentSort = Best -- literally 'confidence' in the json
                 | Old
                 | Top
                 | QA -- as in 'question and answer'
                 | Controversial 
                 | New
                 deriving (Show, Read, Eq, Generic)

instance FromJSON CommentSort where
    parseJSON = withText "CommentSort"
                $ \t -> case t of
                          "confidence"    -> pure Best
                          "old"           -> pure Old
                          "top"           -> pure Top
                          "qa"            -> pure QA
                          "controversial" -> pure Controversial
                          "new"           -> pure New
                          s               -> fail $ show s <> " is not a valid comment sort"

data WikiMode = Disabled
              | ModOnly
              | Anyone
              deriving (Show, Read, Eq, Generic)

instance FromJSON WikiMode where
    parseJSON = genericParseJSON aesonDecodeOptions

newtype SubredditName = R Text
                        deriving (Show, Read, Eq, Generic, FromJSON, ToHttpApiData)

--data SubredditParam = FrontPage
--                    | R Text
--                    | AdHocMulti [Text]
--                    | SavedMulti Text
--                    deriving (Generic, Show, Read, Eq)
--
--instance ToHttpApiData SubredditParam where
--    toUrlPiece FrontPage = T.empty
--    toUrlPiece (R t) = T.append "r/" t
--    toUrlPiece (AdHocMulti ts) = T.append "r/" $ T.intercalate "+" ts
--    toUrlPiece (SavedMulti t)  = T.append "me/m/" t
--
--    toEncodedUrlPiece (R t) = "r/" <> encodePathSegmentsRelative [t]
--    toEncodedUrlPiece (AdHocMulti ts) = "r/" <> foldl' (\acc x -> acc <> "+" <> x) ""  
--                                                       (map encodePathSegmentsRelative (pure <$> ts))
--    toEncodedUrlPiece (SavedMulti t)  = "me/m/" <> encodePathSegmentsRelative [t]
--    toEncodedUrlPiece FrontPage = ""

data SpamLevel = Low
               | High
               | All
               deriving (Show, Read, Eq, Generic)

instance FromJSON SpamLevel where
    parseJSON = genericParseJSON aesonDecodeOptions

data ContentOptions = Any
                    | Link
                    | Self
                    deriving (Show, Read, Eq, Generic)

instance FromJSON ContentOptions where
    parseJSON = genericParseJSON aesonDecodeOptions

data Language = Af -- ^ Afrikaans
              | Ar -- ^ Arabic
              | Be -- ^ Belarusian
              | Bg -- ^ Bulgarian
              | Bs -- ^ Bosnian
              | Ca -- ^ Catalan
              | Cs -- ^ Czech
              | Eo -- ^ Esperanto
              | En -- ^ English
              | Es -- ^ Spanish
              | Et -- ^ Estonian
              | Eu -- ^ Basque
              | Fa -- ^ Farsi
              | Fi -- ^ Finnish
              | Fr -- ^ French
              | Gd -- ^ Gaelic
              | Gl -- ^ Galacian
              | He -- ^ Hebrew
              | Hi -- ^ Hindi
              | Hr -- ^ Croatian
              | Hu -- ^ Hungarian
              | Hy -- ^ Armenian
              | Id -- ^ Indonesian
              | Is -- ^ Icelandic
              | It -- ^ Italian
              | Ja -- ^ Japanese
              | Ko -- ^ Korean
              | La -- ^ Latin
              | Lt -- ^ Lithuanian
              | Lv -- ^ Latvian
              | Ms -- ^ Malay
              | Nl -- ^ Dutch
              | Nn -- ^ Norwegian (Nynorsk)
              | No -- ^ Norwegian
              | Pl -- ^ Polish
              | Pt -- ^ Portuguese
              | Ro -- ^ Romanian
              | Ru -- ^ Russian
              | Sk -- ^ Slovak
              | Sl -- ^ Slovenian
              | Sr -- ^ Serbian
              | Sv -- ^ Swedish
              | Ta -- ^ Tamil
              | Th -- ^ Thai
              | Tr -- ^ Turkish
              | Uk -- ^ Ukrainian
              | Vi -- ^ Vietnamese
              | Zh -- ^ Chinese
              deriving (Show, Read, Eq, Generic)

instance FromJSON Language where
    parseJSON = genericParseJSON aesonDecodeOptions

data SubredditType = Private
                   | Restricted
                   | Public
                   | Premium -- ^ TODO: I don't have gold so I had to guess on this one
                   deriving (Show, Read, Eq, Generic)

instance FromJSON SubredditType where
    parseJSON = genericParseJSON aesonDecodeOptions

data SubredditSettings
  = SubredditSettings { defaultSet :: Bool
                      , id         :: Thing Subreddit'
                      , domain     :: Maybe Text -- ^ I have no idea what this is; I've only ever seen it null
                      , allowImages :: Bool
                      , freeFormReports :: Bool
                      , showMedia :: Bool
                      , wikiEditAge :: Int
                      , submitText :: Maybe Text
                      , spamLinks :: SpamLevel
                      , title :: Text
                      , collapseDeletedComments :: Bool
                      , wikimode :: WikiMode
                      , over18 :: Bool
                      , allowVideos :: Bool
                      , spoilersEnabled :: Bool
                      , suggestedCommentSort :: Maybe CommentSort
                      , originalContentTagEnabled :: Bool
                      , description :: Maybe Text -- ^ actually sidebar
                      , submitLinkLabel :: Maybe Text
                      , allowCrossposts :: Bool
                      , spamComments :: SpamLevel
                      , publicTraffic :: Bool
                      , submitTextLabel :: Maybe Text
                      , allOriginalContent :: Bool
                      , spamSelfposts :: SpamLevel
                      , keyColor :: Text
                      , language :: Language
                      , wikiEditKarma :: Int
                      , hideAds :: Bool
                      , headerHoverText :: Maybe Text
                      , allowDiscovery :: Bool
                      , publicDescription :: Maybe Text -- ^ actual description
                      , showMediaPreview :: Bool
                      , commentScoreHideMinutes :: Int
                      , subredditType :: SubredditType
                      , excludeBannedFromModqueue :: Bool
                      , contentOptions :: ContentOptions
                      } deriving (Show, Read, Eq, Generic)

instance FromJSON SubredditSettings where
    parseJSON = withObject "SubredditSettings" $ \r -> do
      o <- r .: "data"
      defaultSet <- o .: "default_set"
      id <- o .: "subreddit_id"
      domain <- o .:? "domain"
      allowImages <- o .: "allow_images"
      freeFormReports <- o .: "free_form_reports"
      showMedia <- o .: "show_media"
      wikiEditAge <- o .: "wiki_edit_age"
      submitText <- o .:? "submit_text" >>= emptyTextAsNothing
      spamLinks <- o .: "spam_links"
      title <- o .: "title"
      collapseDeletedComments <- o .: "collapse_deleted_comments"
      wikimode <- o .: "wikimode"
      over18 <- o .: "over_18"
      allowVideos <- o .: "allow_videos"
      spoilersEnabled <- o .: "spoilers_enabled"
      suggestedCommentSort <- o .:? "suggested_comment_sort"
      originalContentTagEnabled <- o .: "original_content_tag_enabled"
      description <- o .:? "description" >>= emptyTextAsNothing
      submitLinkLabel <- o .:? "submit_link_label" >>= emptyTextAsNothing
      allowCrossposts <- o .: "allow_post_crossposts"
      spamComments <- o .: "spam_comments"
      publicTraffic <- o .: "public_traffic"
      submitTextLabel <- o .:? "submit_text_label" >>= emptyTextAsNothing
      allOriginalContent <- o .: "all_original_content"
      spamSelfposts <- o .: "spam_selfposts"
      keyColor <- o .: "key_color"
      language <- o .: "language"
      wikiEditKarma <- o .: "wiki_edit_karma"
      hideAds <- o .: "hide_ads"
      headerHoverText <- o .:? "header_hover_text" >>= emptyTextAsNothing
      allowDiscovery <- o .: "allow_discovery"
      publicDescription <- o .:? "public_description" >>= emptyTextAsNothing
      showMediaPreview <- o .: "show_media_preview"
      commentScoreHideMinutes <- o .: "comment_score_hide_mins"
      subredditType <- o .: "subreddit_type"
      excludeBannedFromModqueue <- o .: "exclude_banned_modqueue"
      contentOptions <- o .: "content_options"
      pure $ SubredditSettings{..}

data Subreddit

instance IsThingType Subreddit where
    type AsThingType Subreddit = Subreddit'
    validateThing = T.isPrefixOf "t5_"
