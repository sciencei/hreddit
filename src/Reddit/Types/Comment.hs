{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}

module Reddit.Types.Comment where

import Reddit.Types.Misc
import Data.Text (Text)
import GHC.Generics (Generic)
import Data.Aeson.Types (Object, FromJSON(..))
import Data.Aeson ((.:), (.:?), withObject)
import qualified Data.Text as T

data Comment = Comment
  { id :: Thing Comment'
  , subreddit :: Text
  , subredditId :: Thing Subreddit'
  , postId :: Thing Post'
  , authorId :: Thing User'
  , author :: Text
  , parentId :: Maybe (Thing Comment')
  , body :: Text
  , postTitle :: Text
  , authorFlairCssClass :: Maybe Text
  , authorFlairText :: Maybe Text
  , created :: UTCPosix
  , isStickied :: Bool
  , isRemoved :: Maybe Bool
  , isApproved :: Maybe Bool
  , isScoreHidden :: Bool
  , score :: Int
  , postNumComments :: Int
  , permalink :: Text
  , postPermalink :: Text
  , _raw :: Object
  } deriving (Show, Read, Eq, Generic)

instance IsThingType Comment where
    type AsThingType Comment = Comment'
    validateThing = T.isPrefixOf "t1_"

instance FromJSON Comment where
    parseJSON = withObject "Post" $ \r -> do
      _raw <- r .: "data"
      id         <- _raw .: "name"
      subreddit  <- _raw .: "subreddit"
      subredditId <- _raw .: "subreddit_id"
      postId <- _raw .: "link_id"
      authorId <- _raw .: "author_fullname"
      author <- _raw .: "author"
      parentId <- _raw .:? "parent_id"
      body <- _raw .: "body"
      postTitle <- _raw .: "link_title"
      authorFlairCssClass <- _raw .:? "author_flair_css_class"
      authorFlairText <- _raw .:? "author_flair_text"
      created       <- _raw .: "created_utc"
      isStickied <- _raw .: "stickied"
      isScoreHidden <- _raw .: "score_hidden"
      isRemoved <- _raw .:? "removed"
      isApproved <- _raw .:? "approved"
      score           <- _raw .: "score"
      postNumComments <- _raw .: "num_comments"
      permalink <- T.append "https://reddit.com" <$> _raw .: "permalink"
      postPermalink <- (_raw .: "link_permalink")
      pure $ Comment{..}

