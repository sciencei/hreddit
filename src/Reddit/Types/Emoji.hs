{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Reddit.Types.Emoji where

import Data.Text (Text)
import GHC.Generics (Generic)
import Data.Aeson.Types (FromJSON(..), ToJSON, listParser)
import Data.Aeson ((.:), withObject)
import Reddit.Types.Misc (Thing, ThingType(User'), mkThing)
import Web.HttpApiData (ToHttpApiData(..))
import Data.HashMap.Strict as H
import Data.String (IsString)
import Reddit.Types.Subreddit (Subreddit)
import Data.Maybe (isJust)
import qualified Data.Text as T
import Servant.Multipart (ToMultipart(..), MultipartData(..), Tmp, Input(..))

-- | Basic information about an emoji, as provided in a 'RichText'
data EmbeddedEmoji = EmbeddedEmoji 
  { imageLink :: Text
  , name :: EmojiName
  } deriving (Show, Read, Eq, Generic)

-- | Full information about an emoji, as provided in a listing of just emojis
data Emoji = Emoji { imageLink :: Text
                   , name :: EmojiName
                   , createdBy :: Thing User'
                   , userFlairAllowed :: Bool
                   , postFlairAllowed :: Bool
                   , modFlairOnly :: Bool
                   } deriving (Show, Read, Eq, Generic)

instance FromJSON EmbeddedEmoji where
  parseJSON = withObject "Emoji" $ \o -> do
      imageLink <- o .: "a"
      name      <- EmojiName <$> T.filter (not . (== ':')) <$> o .: "u"
      pure EmbeddedEmoji{..}

instance {-# OVERLAPS #-} FromJSON [Emoji] where
  parseJSON = withObject "Emoji List" $ \o -> do
      let 
        mkEmoji (EmojiName -> name) = withObject "Emoji" $ \e -> do
          imageLink <- e .: "url"
          createdBy <- e .: "created_by"
          userFlairAllowed <- e .: "user_flair_allowed"
          postFlairAllowed <- e .: "post_flair_allowed"
          modFlairOnly <- e .: "mod_flair_only"

          pure Emoji{..}

      sequence $ H.foldlWithKey' (\es n v -> (mkEmoji n v):es) [] o

-- | Name of an emoji (does not include leading and trailing :)
newtype EmojiName = EmojiName {unEmojiName :: Text}
                deriving (Show, Read, Eq, Generic, ToHttpApiData, FromJSON, ToJSON, IsString)

-- | Response returned from Emoji endpoints
newtype Errors = Errors { unErrors :: [Text] }
               deriving (Show, Read, Eq, Generic)

instance FromJSON Errors where
  parseJSON = withObject "Errors" $ \o -> Errors <$> ((o .: "json") >>= (.: "errors"))

-- | The emojis available in a subreddit
data EmojiSet = EmojiSet
              { snoomojis :: [Emoji] -- ^ reddit-wide emojis
              , subEmojis :: [Emoji] -- ^ subreddit-specific emojis
              } deriving (Show, Read, Eq, Generic)

instance FromJSON EmojiSet where
  parseJSON = withObject "EmojiSet" $ \o -> do
    snoomojis <- o .: "snoomojis"
    let
      onlySubreddit = filterWithKey (\k _ -> isJust $ mkThing @Subreddit k) o
    case H.keys onlySubreddit of
      [] -> fail $ "key for subreddit not present"
      [k] -> onlySubreddit .: k >>= (\subEmojis -> pure EmojiSet{..})
      ls -> fail $ "found multiple keys representing subreddit emojis"

-- | The S3 bucket lease obtained for uploading emojis.
data S3Lease = S3Lease 
             { action :: Text -- ^ The host to query
             , fields :: H.HashMap Text Text -- ^ Returned fields, which must be passed 
                                             -- in the body of the request to aws
             } deriving (Show, Read, Eq, Generic)

instance ToMultipart Tmp S3Lease where
  toMultipart S3Lease{..} = MultipartData (toInputs fields) []
    where
      toInputs = H.foldlWithKey' (\acc k v -> Input k v:acc) []

instance FromJSON S3Lease where
  parseJSON = withObject "S3Lease" $ \o -> do
    inner <- o .: "s3UploadLease"
    action <- T.drop 2 <$> inner .: "action"
    let
      marshalField = withObject "S3Lease Field" $ \f -> do
        l <- f .: "name"
        r <- f .: "value"
        pure (l, r)
    fields <- H.fromList <$> (inner .: "fields" >>= listParser marshalField)
    pure S3Lease{..}

-- | The S3 key used for uploading to s3, and for setting the subreddit emoji to the 
-- image in the s3 bucket.
newtype S3Key = S3Key { unS3Key :: Text }
              deriving (Show, Read, Eq, Generic, ToHttpApiData)
