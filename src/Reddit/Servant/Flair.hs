{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE LiberalTypeSynonyms #-}

module Reddit.Servant.Flair
  ( FlairApi(..)
  ) where

import GHC.Generics (Generic)
import Servant.API.Generic ((:-), ToServant, AsApi, fromServant, toServant)
import Servant.API ((:>), Post, Patch, JSON, Get, NoContent)
import Servant.API.NamedArgs (RequiredNamedParam, OptionalNamedParam, NamedFlag, NamedCapture, NamedBody)
import Servant.Client.Core (HasClient(..), RunClient(..))
import Servant.Client.Generic (AsClientT)
import Data.Text (Text)
import Data.Proxy (Proxy(..))

import Reddit.Types.Flair
import Reddit.Types.Misc
import Reddit.Types.Subreddit
import Reddit.Types.User
import Reddit.Servant.Internal.Misc

data FlairApi route = FlairApi
  { 
  -- | #flairSelectorLink#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairselector POST /api/flairselector>
  --   [@Description@]
  --       Returns the link flair options in the subreddit the post is in, as well as the
  --       current flair of the given post.
  --   [@Required Parameters@]
  --
  --       [@link :: 'Thing' 'Post''@] The post to find possible flairs fo
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'FlairSelector' 'LinkFlair'
  --   [@Notes@]
  --       The results returned are /relative to the/
  --       /querying user/. Notably, this means it will show no choices if
  --       not authorized to set flair in the subreddit, and will show all
  --       flairs as having editable text if a mod with flair permissions.
  --       Throws 'Reddit.Types.Error.Forbidden' if link is a post not
  --       created by the logged in user, and the logged in user does not
  --       have mod permissions to set flairs. See also 'flairSelectorUser' for user flairs,
  --       and 'selectFlair' for setting a flair found via this endpoint.
    flairSelectorLink :: route :- "api"
                               :> "flairselector"
                               :> RequiredNamedParam "link" (Thing Post')
                               :> Post '[JSON] (FlairSelector LinkFlair)
  -- | #flairSelectorUser#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairselector POST /r/{subreddit}/api/flairselector>
  --   [@Description@]
  --       Returns the user flair options in the subreddit, as well as the
  --       current flair of the user.
  --   [@Required Parameters@]
  --       None
  --   [@Optional Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --       [@name :: 'Text'@] THe user to get flair options for. If
  --       omitted, defaults to the logged in user.
  --
  --   [@Returns@]
  --       'FlairSelector' 'UserFlair'
  --   [@Notes@]
  --       The results returned are /relative to the/
  --       /querying user/. Notably, this means it will show no choices if
  --       not authorized to set flair in the subreddit, and will show all
  --       flairs as having editable text if a mod with flair permissions.
  --       Throws 'Reddit.Types.Error.Forbidden' if user is not 
  --       the logged in user, and the logged in user does not
  --       have mod permissions to set flairs. See also 'flairSelectorLink'
  --       for link flairs,
  --       and 'selectFlair' for setting a flair found via this endpoint.
  , flairSelectorUser :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                      :> "api"
                                      :> "flairselector"
                                      :> OptionalNamedParam "name" User
                                      :> Post '[JSON] (FlairSelector UserFlair)
  -- | #linkFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_api_link_flair GET /r/{subreddit}/api/link_flair>
  --   [@Description@]
  --       Returns a list of the available link flairs for the subreddit,
  --       in the old flair format.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       ['Flair' 'LinkFlair']
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the user is unable to
  --       set link flairs on the subreddit. For a 'safe' version that will
  --       just return an empty list, see 'flairSelectorLink'. For user
  --       flairs, see 'userFlair', and for new reddit flairs, see
  --       'linkFlairV2'.
  , linkFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                              :> "api"
                              :> "link_flair"
                              :> Get '[JSON] [Flair LinkFlair]
  -- | #linkFlairV2#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_api_link_flair_v2 GET /r/{subreddit}/api/link_flair_v2>
  --   [@Description@]
  --       Returns a list of the available link flairs for the subreddit,
  --       in the new flair format.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       ['FlairV2' 'LinkFlair']
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the user is unable to
  --       set link flairs on the subreddit. For a 'safe' version that will
  --       just return an empty list, see 'flairSelectorLink'. For user
  --       flairs, see 'userFlairV2', and for old reddit flairs, see
  --       'linkFlair'.
  , linkFlairV2 :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                :> "api"
                                :> "link_flair_v2"
                                :> Get '[JSON] [FlairV2 LinkFlair]
  -- | #userFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_api_user_flair GET /r/{subreddit}/api/user_flair>
  --   [@Description@]
  --       Returns a list of the available link flairs for the subreddit,
  --       in the old flair format.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       ['Flair' 'UserFlair']
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the user is unable to
  --       set user flairs on the subreddit. For a 'safe' version that will
  --       just return an empty list, see 'flairSelectorUser'. For link
  --       flairs, see 'linkFlair', and for new reddit flairs, see
  --       'userFlairV2'.
  , userFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                              :> "api"
                              :> "user_flair"
                              :> Get '[JSON] [Flair UserFlair]
  -- | #userFlairV2#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#GET_api_user_flair_v2 GET /r/{subreddit}/api/user_flair_v2>
  --   [@Description@]
  --       Returns a list of the available link flairs for the subreddit,
  --       in the new flair format.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       ['FlairV2' 'UserFlair']
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the user is unable to
  --       set user flairs on the subreddit. For a 'safe' version that will
  --       just return an empty list, see 'flairSelectorUser'. For link
  --       flairs, see 'linkFlairV2', and for old reddit flairs, see
  --       'userFlair'.
  , userFlairV2 :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                :> "api"
                                :> "user_flair_v2"
                                :> Get '[JSON] [FlairV2 UserFlair]
  -- | #setFlairVisible#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_setflairenabled POST /r/{subreddit}/api/setflairenabled>
  --   [@Description@]
  --       This endpoint sets whether the logged in user's flair is
  --       visible on the subreddit.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@flair_enabled :: 'Bool'@] Whether or not the flair should be
  --       visible
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  , setFlairVisible :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                    :> "api" 
                                    :> "setflairenabled"
                                    :> NamedFlag "flair_enabled"
                                    :> Post '[JSON] SuccessMessage
  -- | #flairUser#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flair POST /r/{subreddit}/api/flair>
  --   [@Description@]
  --       Sets a users flair on the subreddit, in the old text and css class fashion.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user whose flair will be set
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@text :: 'Text'@] The new flair text
  --       [@css_class :: 'CssClass'@] The new flair css class
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       This is a /mod/ endpoint, and will throw 'Reddit.Types.Error.Forbidden'
  --       if the logged in user is not a mod with the appropriate permissions.
  --       See 'selectUserFlair' for setting a user's own flair as an unprivileged user,
  --       as well as for setting v2 flairs. See 'flairLink' for flairing a post.
  --       The values passed (or omitted) will totally replace 
  --       the user's current flair text and css class.
  , flairUser :: route :- "r" :> NamedCapture "subreddit" SubredditName
                              :> "api"
                              :> "flair"
                              :> OptionalNamedParam "css_class" CssClass
                              :> RequiredNamedParam "name" User
                              :> OptionalNamedParam "text" Text
                              :> Post '[JSON] SuccessMessage
  -- | #flairLink#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flair POST /api/flair>
  --   [@Description@]
  --       Sets a post's flair on the subreddit, in the old text and css class fashion.
  --   [@Required Parameters@]
  --
  --       [@link :: Thing Link'@] The post to set the flair for
  --
  --   [@Optional Parameters@]
  --
  --       [@text :: 'Text'@] The new flair text
  --       [@css_class :: 'CssClass'@] The new flair css class
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       This is a /mod/ endpoint, and will throw 'Reddit.Types.Error.Forbidden'
  --       if the logged in user is not a mod with the appropriate permissions.
  --       See 'selectLinkFlair' for setting a user's own post's flair as an unprivileged user,
  --       as well as for setting v2 flairs. See 'flairUser' for flairing a user.
  --       The values passed (or omitted) will totally replace 
  --       the post's current flair text and css class.
  , flairLink :: route :- "api" :> "flair"
                                :> OptionalNamedParam "css_class" CssClass
                                :> RequiredNamedParam "link" (Thing Post')
                                :> OptionalNamedParam "text" Text
                                :> Post '[JSON] SuccessMessage
  -- | #selectUserFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_selectflair POST /r/{subreddit}/api/selectflair>
  --   [@Description@]
  --       Selects a user's flair on the subreddit. Omitting the id and text will clear the user's flair.
  --       This is the correct endpoint for a user picking their own flair on a subreddit that allows it.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: 'SubredditName'@] The name of the subreddit
  --       [@name :: 'User'@] The user whose flair will be set
  --
  --   [@Optional Parameters@]
  --
  --       [@flair_template_id :: 'FlairTemplateId' 'UserFlair'@] The id of the flair template to give the user
  --       [@text :: 'Text'@] The flair text for the user (will overwrite the template text if provided and allowed)
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       @return_rtson@, @text_color@, and @background_color@ are omitted because they don't seem to do anything
  --       as of 2019-02-26. If a user can set their flair but not edit the text, the text parameter is silently ignored.
  , selectUserFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                    :> "api"
                                    :> "selectflair"
                                    :> RequiredNamedParam "name" User
                                    :> OptionalNamedParam "flair_template_id" (FlairTemplateId UserFlair)
                                    :> OptionalNamedParam "text" Text
                                    :> Post '[JSON] SuccessMessage
  -- | #selectLinkFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_selectflair POST /r/{subreddit}/api/selectflair>
  --   [@Description@]
  --       Selects a post's flair on the subreddit. Omitting the id and text will clear the post's flair.
  --       This is the correct endpoint for a user picking their own post's flair on a subreddit that allows it.
  --   [@Required Parameters@]
  --
  --       [@link :: 'Thing Post''@] The link to set the flair of
  --
  --   [@Optional Parameters@]
  --
  --       [@flair_template_id :: 'FlairTemplateId' 'UserFlair'@] The id of the flair template to give the user
  --       [@text :: 'Text'@] The flair text for the user (will overwrite the template text if provided and allowed)
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       @return_rtson@, @text_color@, and @background_color@ are omitted because they don't seem to do anything
  --       as of 2019-02-26. If a user can set their flair but not edit the text, the text parameter is silently ignored.
  , selectLinkFlair :: route :- "api"
                             :> "selectflair"
                             :> RequiredNamedParam "link" (Thing Post')
                             :> OptionalNamedParam "flair_template_id" (FlairTemplateId LinkFlair)
                             :> OptionalNamedParam "text" Text
                             :> Post '[JSON] SuccessMessage
  -- | #configureFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairconfig POST /r/{subreddit}/api/flairconfig>
  --   [@Description@]
  --       Sets flair options on the subreddit. Note that it sets /all/ flair options; those not passed
  --       are set to their defaults.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@link_flair_self_assign_enabled :: 'Bool'@] Whether users can set their own link flairs
  --       [@link_flair_position :: 'FlairPosition'@]
  --          Link flair position; if not provided, link flair is disabled
  --       [@flair_self_assign_enabled :: 'Bool'@] Whether users can set their own user flairs
  --       [@flair_position :: 'FlairPosition'@]
  --          User flair position; if not provided, user flair is disabled
  --       [@flair_enabled :: 'Bool'@] Whether user flair is enabled
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user does
  --       not have the correct permissions.
  , configureFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                   :> "api" 
                                   :> "flairconfig"
                                   :> OptionalNamedParam "flair_enabled" Bool
                                   :> OptionalNamedParam "flair_position" FlairPosition
                                   :> OptionalNamedParam "flair_self_assign_enabled" Bool
                                   :> OptionalNamedParam "link_flair_position" FlairPosition
                                   :> OptionalNamedParam "link_flair_self_assign_enabled" Bool
                                   :> Post '[JSON] SuccessMessage
  -- | #reorderUserFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#PATCH_api_flair_template_order PATCH /api/flair_template_order?flair_type=USER_FLAIR>
  --   [@Description@]
  --       Reorders the user flair templates in the subreddit.
  --   [@Required Parameters@]
  --
  --       [@ordering :: ['FlairTemplateId' 'UserFlair']@] The new ordering. Any flair templates omitted will be LOST
  --       [@subreddit :: 'SubredditName'@] The subreddit name
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'NoContent'
  --   [@Notes@]
  --       
  , reorderUserFlair :: route :- "api" :> "flair_template_order"
                                       :> SuppliedParam "flair_type" "USER_FLAIR"
                                       :> NamedBody "ordering" '[JSON] [FlairTemplateId UserFlair]
                                       :> RequiredNamedParam "subreddit" (SubredditName)
                                       :> Patch '[NoContent] NoContent
  -- | #reorderLinkFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#PATCH_api_flair_template_order PATCH /api/flair_template_order?flair_type=LINK_FLAIR>
  --   [@Description@]
  --       Reorders the link flair templates in the subreddit.
  --   [@Required Parameters@]
  --
  --       [@ordering :: ['FlairTemplateId' 'LinkFlair']@] The new ordering. Any flair templates omitted will be LOST
  --       [@subreddit :: 'SubredditName'@] The subreddit name
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'NoContent'
  --   [@Notes@]
  --       
  , reorderLinkFlair :: route :- "api" :> "flair_template_order"
                                       :> SuppliedParam "flair_type" "LINK_FLAIR"
                                       :> NamedBody "ordering" '[JSON] [FlairTemplateId LinkFlair]
                                       :> RequiredNamedParam "subreddit" (SubredditName)
                                       :> Patch '[NoContent] NoContent
  -- | #clearFlairTemplates#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_clearflairtemplates POST /r/{subreddit}/api/clearflairtemplates>
  --   [@Description@]
  --       Clears all flair templates of the given type on the subreddit.
  --   [@Required Parameters@]
  --
  --       [@flair_type :: 'FlairType'@] Link or User flairs
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions.
  , clearFlairTemplates :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                        :> "api"
                                        :> "clearflairtemplates"
                                        :> RequiredNamedParam "flair_type" FlairType
                                        :> Post '[JSON] SuccessMessage
  -- | #unflairUser#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_deleteflair POST /r/{subreddit}/api/deleteflair>
  --   [@Description@]
  --       Deletes the flair of the given user.
  --   [@Required Parameters@]
  --
  --       [@name :: 'User'@] The user whose flair will be deleted
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       This is a __mod__ endpoint, and will throw
  --       'Reddit.Types.Error.Forbidden' if used in a subreddit where the
  --       logged in user does not have flair mod permissions, even if the
  --       user tries to delete their own flair. Use 'selectUserFlair' with
  --       no @flair_template_id@ if you want to remove the logged in
  --       user's own flair without having mod permissions (still requires
  --       being able to manage their own flair on the subreddit).
  , unflairUser :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                :> "api"
                                :> "deleteflair"
                                :> RequiredNamedParam "name" User
                                :> Post '[JSON] SuccessMessage
  -- | #deleteUserFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_deleteflairtemplate POST /api/deleteflairtemplate>
  --   [@Description@]
  --       Delete a user flair template.
  --   [@Required Parameters@]
  --
  --       [@flair_template_id :: 'FlairTemplateId' 'UserFlair'@] The id of
  --       the template to delete
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions.
  , deleteUserFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                    :> "api"
                                    :> "deleteflairtemplate"
                                    :> RequiredNamedParam "flair_template_id" (FlairTemplateId UserFlair)
                                    :> Post '[JSON] SuccessMessage
  -- | #deleteLinkFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_deleteflairtemplate POST /api/deleteflairtemplate>
  --   [@Description@]
  --       Delete a link flair template.
  --   [@Required Parameters@]
  --
  --       [@flair_template_id :: 'FlairTemplateId' 'LinkFlair'@] The id of
  --       the template to delete
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions.
  , deleteLinkFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                    :> "api"
                                    :> "deleteflairtemplate"
                                    :> RequiredNamedParam "flair_template_id" (FlairTemplateId LinkFlair)
                                    :> Post '[JSON] SuccessMessage
  -- | #makeUserFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairtemplate POST /r/{subreddit}/api/flairtemplate?flair_type=USER_FLAIR>
  --   [@Description@]
  --       Creates or updates a user flair template of the old style (css
  --       class and text).
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@text_editable :: 'Bool'@] Whether or not the text is
  --       user-editable
  --       [@text :: 'Text'@] Default text of the flair template
  --       [@flair_template_id :: 'FlairTemplateId' 'UserFlair'@] The id of
  --       the flair; this should generally be omitted for creating a new
  --       flair, and supplied with an existing id when updating a flair
  --       [@css_class :: 'CssClass'@] CSS class of the flair template
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions. See 'makeUserFlairV2' for
  --       new reddit flairs, and 'makeLinkFlair' for link flairs.
  , makeUserFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName 
                                  :> "api"
                                  :> "flairtemplate"
                                  :> OptionalNamedParam "css_class" CssClass
                                  :> OptionalNamedParam "flair_template_id" (FlairTemplateId UserFlair)
                                  :> SuppliedParam "flair_type" "USER_FLAIR"
                                  :> OptionalNamedParam "text" Text
                                  :> OptionalNamedParam "text_editable" Bool
                                  :> Post '[JSON] SuccessMessage
  -- | #makeLinkFlair#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairtemplate POST /r/{subreddit}/api/flairtemplate?flair_type=LINK_FLAIR>
  --   [@Description@]
  --       Creates or updates a link flair template of the old style (css
  --       class and text).
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@text_editable :: 'Bool'@] Whether or not the text is
  --       user-editable
  --       [@text :: 'Text'@] Default text of the flair template
  --       [@flair_template_id :: 'FlairTemplateId' 'LinkFlair'@] The id of
  --       the flair; this should generally be omitted for creating a new
  --       flair, and supplied with an existing id when updating a flair
  --       [@css_class :: 'CssClass'@] CSS class of the flair template
  --
  --   [@Returns@]
  --       'SuccessMessage'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions. See 'makeLinkFlairV2' for
  --       new reddit flairs, and 'makeUserFlair' for user flairs.
  , makeLinkFlair :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                  :> "api"
                                  :> "flairtemplate"
                                  :> OptionalNamedParam "css_class" CssClass
                                  :> OptionalNamedParam "flair_template_id" (FlairTemplateId LinkFlair)
                                  :> SuppliedParam "flair_type" "LINK_FLAIR"
                                  :> OptionalNamedParam "text" Text
                                  :> OptionalNamedParam "text_editable" Bool
                                  :> Post '[JSON] SuccessMessage
  -- | #makeUserFlairV2#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairtemplate_v2 POST /r/{subreddit}/api/flairtemplate_v2?flair_type=USER_FLAIR>
  --   [@Description@]
  --       Creates or updates a user flair template of the new style.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@text_color :: 'FlairTextColor'@] Text color of the flair
  --       template (default: Dark)
  --       [@text_editable :: 'Bool'@] Whether the flair text should be
  --       user-editable
  --       [@text :: 'Text'@] Default text of the template
  --       [@mod_only :: 'Bool'@] Whether the flair is restricted to mods
  --       [@flair_template_id :: 'FlairTemplateId' 'UserFlair'@] The id of the
  --       flair. For a new flair this should be omitted, and for updating
  --       a flair the id of the flair to be updated should be supplied
  --       [@background_color :: 'Color'@] Background color of the flair;
  --       omit for the default grayish background
  --
  --   [@Returns@]
  --       'FlairV2' 'LinkFlair'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions. See 'makeUserFlair' for
  --       old reddit flairs, and 'makeLinkFlairV2' for link flairs.
  , makeUserFlairV2 :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                    :> "api" 
                                    :> "flairtemplate_v2"
                                    :> OptionalNamedParam "background_color" Color
                                    :> OptionalNamedParam "flair_template_id" (FlairTemplateId UserFlair)
                                    :> SuppliedParam "flair_type" "USER_FLAIR"
                                    :> OptionalNamedParam "mod_only" Bool
                                    :> OptionalNamedParam "text" Text
                                    :> OptionalNamedParam "text_editable" Bool
                                    :> OptionalNamedParam "text_color" FlairTextColor
                                    :> Post '[JSON] (FlairV2 LinkFlair)
  -- | #makeLinkFlairV2#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api#POST_api_flairtemplate_v2 POST /r/{subreddit}/api/flairtemplate_v2?flair_type=LINK_FLAIR>
  --   [@Description@]
  --       Creates or updates a link flair template of the new style.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: SubredditName@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@text_color :: 'FlairTextColor'@] Text color of the flair
  --       template (default: Dark)
  --       [@text_editable :: 'Bool'@] Whether the flair text should be
  --       user-editable
  --       [@text :: 'Text'@] Default text of the template
  --       [@mod_only :: 'Bool'@] Whether the flair is restricted to mods
  --       [@flair_template_id :: 'FlairTemplateId' 'LinkFlair'@] The id of the
  --       flair. For a new flair this should be omitted, and for updating
  --       a flair the id of the flair to be updated should be supplied
  --       [@background_color :: 'Color'@] Background color of the flair;
  --       omit for the default grayish Background
  --
  --   [@Returns@]
  --       'FlairV2' 'LinkFlair'
  --   [@Notes@]
  --       Throws a 'Reddit.Types.Error.Forbidden' if the logged in user
  --       doesn't have the right permissions. See 'makeLinkFlair' for
  --       old reddit flairs, and 'makeUserFlairV2' for user flairs.
  , makeLinkFlairV2 :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                    :> "api" 
                                    :> "flairtemplate_v2"
                                    :> OptionalNamedParam "background_color" Color
                                    :> OptionalNamedParam "flair_template_id" (FlairTemplateId LinkFlair)
                                    :> SuppliedParam "flair_type" "LINK_FLAIR"
                                    :> OptionalNamedParam "mod_only" Bool
                                    :> OptionalNamedParam "text" Text
                                    :> OptionalNamedParam "text_editable" Bool
                                    :> OptionalNamedParam "text_color" FlairTextColor
                                    :> Post '[JSON] (FlairV2 LinkFlair)
  -- | #massFlairUser#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#POST_api_flaircsv POST /r/{subreddit}/api/flaircsv>
  --   [@Description@]
  --       Sets the flairs of up to 100 users, in the old flair style.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: 'SubredditName'@] The name of the subreddit
  --       [@flair_csv :: ['OldFlair']@] A list of flair assignments. Setting both 'Reddit.Types.Flair.OldFlair.text' 
  --         and 'Reddit.Types.Flair.OldFlair.cssClass' to empty strings will clear the flair instead.
  --
  --   [@Optional Parameters@]
  --       None
  --   [@Returns@]
  --       ['FlairUpdateResult']
  --   [@Notes@]
  --       There does not, to my knowledge, exist an equivalent endpoint to bulk set V2 flairs. See
  --       'selectUserFlair' to set V2 flairs individually. Including " in the 'OldFlair' will break
  --       the request, as they are a reserved character in the encoding and there does not seem to be a way to
  --       escape them.
  , massFlairUser :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                  :> "api"
                                  :> "flaircsv"
                                  :> RequiredNamedParam "flair_csv" [OldFlair]
                                  :> Post '[JSON] [FlairUpdateResult]
  -- | #listUserFlairs#
  --
  --   [@Endpoint@]
  --       <https://www.reddit.com/dev/api/#GET_api_flairlist GET /r/{subreddit}/api/flairlist>
  --   [@Description@]
  --       Lists the flairs (in the old style) of users in the subreddit.
  --   [@Required Parameters@]
  --
  --       [@subreddit :: 'SubredditName'@] The name of the subreddit
  --
  --   [@Optional Parameters@]
  --
  --       [@name :: 'User'@] Only get flairs for this user
  --       [@show :: 'ShowAll'@] Show hidden users (likely not applicable for this listing, but may apply to blocked users)
  --       [@count :: 'Int'@] Flairs already seen in this listing
  --       [@limit :: 'Int'@] Number of results to return (max 100)
  --       [@index :: 'Index' 'User''@] Entry to get flairs before or after
  --
  --   [@Returns@]
  --       'Listing' 'OldFlair'
  --   [@Notes@]
  --       There does not currently exist a v2 equivalent. You can find a specific user's flair with 'flairSelectorUser'
  --       though.
  , listUserFlairs :: route :- "r" :> NamedCapture "subreddit" SubredditName
                                   :> "api"
                                   :> "flairlist"
                                   :> OptionalNamedParam "name" User
                                   :> ListingOptions User'
                                   $> Get '[JSON] (Listing OldFlair)
  } deriving (Generic)


instance (RunClient m, HasClient m (ToServant FlairApi AsApi)) => HasClient m (FlairApi (AsId as)) where
    type Client m (FlairApi (AsId as)) = FlairApi (AsClientT m)
    clientWithRoute m _ = fromServant . clientWithRoute m (Proxy @(ToServant FlairApi AsApi))
    hoistClientMonad m _ nt = fromServant . hoistClientMonad m (Proxy @(ToServant FlairApi AsApi)) nt . toServant

instance (RunClient m, HasClient m (MkRaw (ToServant FlairApi AsApi)))
         => HasClient m (FlairApi (AsRaw as)) where
    type Client m (FlairApi (AsRaw as)) = FlairApi (AsRaw (AsClientT m))
    clientWithRoute m _ = fromServant . clientWithRoute m (Proxy @(MkRaw (ToServant FlairApi AsApi)))
    hoistClientMonad m _ nt = fromServant . hoistClientMonad m (Proxy @(MkRaw (ToServant FlairApi AsApi))) nt . toServant
